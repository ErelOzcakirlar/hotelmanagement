/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Interface;

import com.DAO.RoomTypeDao;
import com.entities.RoomType;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@SessionScoped
public class RoomTypeBean {

    String id;
    String evenBed;
    String oddBed;
    String bunkBed;
    String bedCount;
    String prizePerDay;
    String description;
    String photo1url;
    String photo2url;
    String photo3url;

    RoomType temp;

    ArrayList<RoomType> typeList = new ArrayList<RoomType>();

    public RoomTypeBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEvenBed() {
        return evenBed;
    }

    public void setEvenBed(String evenBed) {
        this.evenBed = evenBed;
    }

    public String getOddBed() {
        return oddBed;
    }

    public void setOddBed(String oddBed) {
        this.oddBed = oddBed;
    }

    public String getBunkBed() {
        return bunkBed;
    }

    public void setBunkBed(String bunkBed) {
        this.bunkBed = bunkBed;
    }

    public String getBedCount() {
        return bedCount;
    }

    public void setBedCount(String bedCount) {
        this.bedCount = bedCount;
    }

    public String getPrizePerDay() {
        return prizePerDay;
    }

    public void setPrizePerDay(String prizePerDay) {
        this.prizePerDay = prizePerDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto1url() {
        return photo1url;
    }

    public void setPhoto1url(String photo1url) {
        this.photo1url = photo1url;
    }

    public String getPhoto2url() {
        return photo2url;
    }

    public void setPhoto2url(String photo2url) {
        this.photo2url = photo2url;
    }

    public String getPhoto3url() {
        return photo3url;
    }

    public void setPhoto3url(String photo3url) {
        this.photo3url = photo3url;
    }

    public ArrayList<RoomType> getTypeList() {
        return typeList;
    }

    public void setDepList(ArrayList<RoomType> typeList) {
        this.typeList = typeList;
    }

    public void edit(String id) {
        temp = new RoomTypeDao().selectOne(Integer.parseInt(id)).get(0);
        show();
    }

    public void delete(int id) {
        new RoomType().delete(id);
    }

    public void show() {
        if (temp == null) {
            return;
        }

        id = String.valueOf(temp.getId());
        bedCount = String.valueOf(temp.getBedCount());
        oddBed = String.valueOf(temp.getOddBed());
        evenBed = String.valueOf(temp.getEvenBed());
        bunkBed = String.valueOf(temp.getBunkBed());
        prizePerDay = String.valueOf(temp.getPrizePerDay());
        description = temp.getDescription();
        photo1url = temp.getPhoto1url();
        photo2url = temp.getPhoto2url();
        photo3url = temp.getPhoto3url();

    }

    public void save() {
        RoomTypeDao manipulator = new RoomTypeDao();
        try{
          manipulator.setId(Integer.parseInt(id));  
        }catch(Exception e){
           manipulator.setId(0);
           id="0";
        }     
        manipulator.setBedCount(Integer.parseInt(bedCount));
        manipulator.setOddBed(Integer.parseInt(oddBed));
        manipulator.setEvenBed(Integer.parseInt(evenBed));
        manipulator.setBunkBed(Integer.parseInt(bunkBed));
        manipulator.setPrizePerDay(new BigDecimal(prizePerDay));
        manipulator.setDescription(description);
        manipulator.setPhoto1url(photo1url);
        manipulator.setPhoto2url(photo2url);
        manipulator.setPhoto3url(photo3url);
       RoomType temp2= null;
        try{
             temp2 = new RoomTypeDao().selectOne(Integer.parseInt(id)).get(0); 
        } catch(Exception e){
            
        }
       
        if (temp2 == null) {
            manipulator.setId(0);
            manipulator.insert();
        } else {
            manipulator.update();
        }
        AllRoomTypes();
                    id="0";
    }

    public void AllRoomTypes() {
        typeList = new RoomTypeDao().select();
    }
}
