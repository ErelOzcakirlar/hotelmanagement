/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author UGUR
 */
@ManagedBean
@RequestScoped
public class ImageSearchBean {
    public String text="";
    public String url="http://yandex.com.tr/gorsel/search?text=";
    public final String USER_AGENT = "Mozilla/5.0";
         ArrayList<String> titles = new ArrayList<String>();

    public ArrayList<String> getTitles() {
        return titles;
    }

    public void setTitles(ArrayList<String> titles) {
        this.titles = titles;
    }
    /**
     * Creates a new instance of ImageSearchBean
     */
    public ImageSearchBean() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    
    public void GoWeb(){
        if(text.contains(" ")){
            text=text.replace(" ", "%20");
        }
       titles = new ArrayList<String>();
        StringBuffer response = new StringBuffer();
        try{
            
 
		URL obj = new URL(url+text);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		// optional default is GET
		con.setRequestMethod("GET");
 
		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
 
		int responseCode = con.getResponseCode();
	
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
                      //  titles.add(response.toString()); 
   Matcher matcher = Pattern.compile("&quot;href&quot;:&quot;(.*?)&quot;").matcher(response.toString());
while(matcher.find()){
    titles.add(matcher.group(1));
}
        System.out.println();
        }catch(Exception e){}
      
      if(titles==null){
          titles= new ArrayList<String>();
      }

       
        
    }
    
    
    
    
}
