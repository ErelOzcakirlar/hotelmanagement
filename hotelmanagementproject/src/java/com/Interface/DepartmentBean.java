/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import com.DAO.DepartmentDAO;
import com.entities.Department;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@SessionScoped
public class DepartmentBean {

    String ID;
    String Name;
    String Description;
    String EmployeeCount;
    
    Department temp;
    
    ArrayList<Department> depList = new ArrayList<Department>();
    
    public DepartmentBean() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getEmployeeCount() {
        return EmployeeCount;
    }

    public void setEmployeeCount(String EmployeeCount) {
        this.EmployeeCount = EmployeeCount;
    }

    public ArrayList<Department> getDepList() {
        return depList;
    }

    public void setDepList(ArrayList<Department> depList) {
        this.depList = depList;
    }
    
    public void edit(String id) {
        temp = DepartmentDAO.selectOne(Integer.parseInt(id));
        show();
    }
    
    public void delete(int id) {
         Department temp2= new Department(id);
         DepartmentDAO.delete(temp2);
         AllDepartments();
    }

    public void show() {
        if (temp == null) {
            return;
        }
        
        Name = temp.Name;
        Description = temp.Description;
        EmployeeCount = String.valueOf(temp.EmployeeCount);

    }

    public void save() {
        int tempId;
        try{
            tempId = Integer.parseInt(ID);
        }catch(NumberFormatException nfe){
            tempId = 0;
        }
        temp = new Department(tempId);
        temp.Name = Name;
        temp.Description = Description;
        int tempCount;
        try{
            tempCount = Integer.parseInt(ID);
        }catch(NumberFormatException nfe){
            tempCount = 0;
        }
        temp.EmployeeCount = tempCount;
        
        Department temp2 = DepartmentDAO.selectOne(tempId);
        if(temp2 == null){
            DepartmentDAO.insert(temp);
        }else{
            DepartmentDAO.update(temp);
        }
        AllDepartments();
        ID = "0";
    }

    public void AllDepartments() {
        depList = DepartmentDAO.select();
    }
}
