/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author UGUR
 */
@ManagedBean
@SessionScoped
public class GeneralBean {

    /**
     * Creates a new instance of GeneralBean
     */
    public GeneralBean() {
    }
    
     public String mainlinkwithicon (String icon , String name ){
            return "<i class=\""+icon+"\"></i><span class=\"title\">"+name+"</span>";
        }
    
     
}
