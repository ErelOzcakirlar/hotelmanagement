/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import com.DAO.EmployeeDAO;
import com.entities.Employee;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author UGUR
 */
@ManagedBean(name = "LoginBean")
@SessionScoped
public class LoginBean {
    public String tempID=""; // temp id on login
    public String Pass="";   // temp pass on logined
    public String ID="";     // id after login
    public String Username="Deneme Soyadı"; // Username After login
    public Boolean Logined=false; // is login check
    public int Logintryed=0; // is login check
    public String Pagename="main.xhtml";   // the main page after logined
    public int DepartmentID = 1;
    Employee Person= new Employee();
   // Temp Person Profile For Edit
    private String T_name="";
    private String T_surname;
    private String T_phone;
    private String T_Salary;
    private String T_about;
    private String T_password;
    private String T_password2;

    
    public void getProfile(){
        T_name = Person.Name;
T_surname = Person.Surname;
T_phone =   Person.Phone;
T_Salary =   Person.Salary.toString();
T_about = Person.JobDescription;
T_password = Person.getPassword();
    }
    
    public String getDepartmentEmployee(){
        System.out.println("Departman id si alındı LoginBean dan sonuc :"+Person.DepartmentID);
        return String.valueOf(Person.DepartmentID);
    }
    
    public String saveProfile(){
Person.Phone=T_phone;
if(T_password.equals(T_password2))
Person.setPassword(T_password);
EmployeeDAO.update(Person); // update user profile
        return "index.xhtml";
    }
    
    public String getPagename() {
        return Pagename;
        
    }

    public void setPagename(String Pagename) {
        this.Pagename = Pagename;
    }
    
    public String getTempID() {
        return tempID;
    }

    public void setTempID(String tempID) {
        this.tempID = tempID;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }
    
        public String setPage(String num){
        this.Pagename=num;
        return "index.xhtml";
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }
    
    public LoginBean() {
        
    }
    
     public String isLogin(){
         if(!Logined)
        Login();
        if(Logined)
            return "index2.xhtml";
       return "login.xhtml";
    }
    
    public void Login(){
     if(      Logined= Employee.login(tempID,Pass)){
          ID=tempID;
         Person= new Employee(tempID,Pass);
       Person= EmployeeDAO.selectOne(ID);
       if(Person!=null){
           DepartmentID = Person.DepartmentID;
           Username=Person.Name+" "+Person.Surname;  
       }       
         Logintryed=0;
     }
     else{
         Logintryed++; // tryed to login
     }
        
          
    }
    
    
    public void Logout(){
        Logintryed=0;
        Logined=false;
        tempID=""; // temp id on login
        Pass="";   // temp pass on logined
        ID="";     // id after login
    }
    
    public String ShowError(){
        if(Logintryed>1)
        return "<div id=\"forget\" class=\"alert-box error\"><span>Hata: </span>Hatalı Giriş</bold></bold></div>";
        return "";
    }
    
    
    public String unique(int sira){
        if((sira==1 || sira==2 || sira==3 ||sira==4 ||sira==5)&& (DepartmentID==4 || DepartmentID==5 || DepartmentID==6))
            return "hidden";
        if(sira==6 || sira==7)
           return "";
        if((sira==8)&& (DepartmentID==1 || DepartmentID==4 || DepartmentID==5 || DepartmentID==6 ))
            return "hidden";
        if((sira==9 || sira==10 ||sira==11 )&& (DepartmentID==1 || DepartmentID==2 || DepartmentID==4 || DepartmentID==5 || DepartmentID==6 ))
            return "hidden";
        return "";
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public String getT_password2() {
        return T_password2;
    }

    public void setT_password2(String T_password2) {
        this.T_password2 = T_password2;
    }

    public String getT_name() {
        return T_name;
    }

    public void setT_name(String T_name) {
        this.T_name = T_name;
    }

    public String getT_surname() {
        return T_surname;
    }

    public void setT_surname(String T_surname) {
        this.T_surname = T_surname;
    }

    public String getT_phone() {
        return T_phone;
    }

    public void setT_phone(String T_phone) {
        this.T_phone = T_phone;
    }

    public String getT_Salary() {
        return T_Salary;
    }

    public void setT_Salary(String T_Salary) {
        this.T_Salary = T_Salary;
    }

    public String getT_about() {
        return T_about;
    }

    public void setT_about(String T_about) {
        this.T_about = T_about;
    }

    public String getT_password() {
        return T_password;
    }

    public void setT_password(String T_password) {
        this.T_password = T_password;
    }
    
}
