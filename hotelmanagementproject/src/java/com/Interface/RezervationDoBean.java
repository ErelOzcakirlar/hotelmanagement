/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import com.DAO.BillDAO;
import com.DAO.CustomerDAO;
import com.DAO.ReservationDao;
import com.DAO.RoomDao;
import com.entities.Bill;
import com.entities.Customer;
import com.entities.Reservation;
import com.entities.Room;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author EMRE
 */
@ManagedBean
@SessionScoped
public class RezervationDoBean {
    private String Names;
    private String Surnames;
    private String Phones;
    private String Bdays;
    private String Tcs;
    private String Mails;
    private String CheckinDate;
    private String CheckoutDate;
    private String Rooms;
    private String PersonNumber;
    
    
    ReservationDao rezerv = new ReservationDao();
    RoomDao roomd= new RoomDao();
    ArrayList<Customer> Customers= new ArrayList<Customer>();
    ArrayList<Reservation> Reservations = new ArrayList<Reservation>();
    Bill bill = new Bill();
    int billID;
    /**
     * Creates a new instance of RezervationDoBean
     */
    public RezervationDoBean() {
    }
    
    public void writePrice(){
        loadCustomers();
        saveCustomers();
        saveBill();
       try{
        String[] id_arr = Rooms.split(",");
        Date checkInDate = rezerv.formatMe(CheckinDate);
        Date checkOutDate = rezerv.formatMe(CheckoutDate);
           System.out.println("boldu ");
         for (int i = 0; i < Customers.size(); i++) { 
             System.out.println("for a girdi sayac :"+i);
             rezerv = new ReservationDao();
             rezerv.setRoomId(Integer.parseInt(id_arr[i%id_arr.length]));
             System.out.println("gidecek oda :"+Integer.parseInt(id_arr[i%id_arr.length]));
             rezerv.setCustomerId(Customers.get(i).getID());
             System.out.println("bak bill id :"+billID);
             rezerv.setBillId(billID);
             
             rezerv.insert();
             System.out.println("Instertledi");
        }   
         rezervRooms();
       }catch(Exception e){
           e.printStackTrace();
       } 
       
         
    }   
    public void loadCustomers(){
       Customers= new ArrayList<Customer>();
        try{
        String[] Name_arr=Names.split(",");
        String[] Surname_arr=Surnames.split(",");
        String[] Bday_arr=Bdays.split(",");
        String[] Tc_arr=Tcs.split(",");
        String[] Mail_arr=Mails.split(",");  
        String[] Phone_arr = Phones.split(",");
        
          for (int  i = 0;  i < Name_arr.length;i++) {
            Customer customer = new Customer(Tc_arr[i],Tc_arr[0],1);
            customer.Name = Name_arr[i];
            customer.Surname = Surname_arr[i];
            customer.Mail = Mail_arr[i];
            customer.Phone = Phone_arr[i];
            customer.BirthDate = rezerv.formatMe(Bday_arr[i]);
            Customers.add(customer);
        }
        
        System.out.println("Dizi boyutu :" + Name_arr.length);
        }catch(Exception e){
        System.out.println("catch e mi dustu");
        }
         
    }
    
    public void saveCustomers(){
        System.out.println("Kullanici sayisi:"+Customers.size());
        for (int i = 0; i < Customers.size(); i++) {
            System.out.println("Kullanici :"+Customers.get(i).Name);
           if(CustomerDAO.selectOne(Customers.get(i).getID()) == null){
               System.out.println("ife girdi");
               CustomerDAO.insert(Customers.get(i));
           }
           else{
               CustomerDAO.update(Customers.get(i));
           }      
        }
    }
    public void saveBill(){
         
           bill=new Bill();
           bill.Leader = Customers.get(0);
           bill.isPayed = false;
           bill.PaymentTime= new Date();
           bill.TotalPrize= new BigDecimal(0);
           bill.PensionID=4; // hersey dahil
           BillDAO.insert(bill);     
           billID = BillDAO.selectLastBillID(Customers.get(0).getID());
           
        }
        
    public void rezervRooms(){
        String[] id_arr = Rooms.split(",");
        for(int i=0;i<id_arr.length;i++){
            ArrayList<Room> temp= new ArrayList<Room>();
            temp=roomd.selectOne(Integer.valueOf(id_arr[i]));
            
           
            roomd.RoomCheckIn(temp.get(0).getId(),rezerv.formatMe(CheckinDate),rezerv.formatMe(CheckoutDate));
            
            
        }
    }
    
  

    public String getNames() {
        return Names;
    }

    public void setNames(String Names) {
        this.Names = Names;
    }

    public String getSurnames() {
        return Surnames;
    }

    public void setSurnames(String Surnames) {
        this.Surnames = Surnames;
    }
    public String getBdays() {
        return Bdays;
    }

    public void setBdays(String Bdays) {
        this.Bdays = Bdays;
    }

    public String getTcs() {
        return Tcs;
    }

    public void setTcs(String Tcs) {
        this.Tcs = Tcs;
    }

    public String getMails() {
        return Mails;
    }

    public void setMails(String Mails) {
        this.Mails = Mails;
    }

    public String getCheckinDate() {
        return CheckinDate;
    }

    public void setCheckinDate(String CheckinDate) {
        this.CheckinDate = CheckinDate;
    }

    public String getCheckoutDate() {
        return CheckoutDate;
    }

    public void setCheckoutDate(String CheckoutDate) {
        this.CheckoutDate = CheckoutDate;
    }

    public String getRooms() {
        return Rooms;
    }

    public void setRooms(String Rooms) {
        this.Rooms = Rooms;
    }

    public String getPersonNumber() {
        return PersonNumber;
    }

    public void setPersonNumber(String PersonNumber) {
        this.PersonNumber = PersonNumber;
    }

    public String getPhones() {
        return Phones;
    }

    public void setPhones(String Phones) {
        this.Phones = Phones;
    }
    
}
