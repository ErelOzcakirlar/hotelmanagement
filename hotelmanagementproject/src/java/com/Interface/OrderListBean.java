/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import com.DAO.BillDAO;
import com.DAO.CustomerDAO;
import com.DAO.OrderDAO;
import com.entities.Customer;
import com.entities.Order;
import com.entities.OrderType;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author pc
 */
@ManagedBean
@SessionScoped
public class OrderListBean {
   ArrayList<Order> Orders= new ArrayList<Order>();
  ArrayList<OrderList> OrderList = new ArrayList<OrderList>();

    public ArrayList<OrderList> getOrderList() {
        return OrderList;
    }

    public void setOrderList(ArrayList<OrderList> OrderList) {
        this.OrderList = OrderList;
    }
    /**
     * Creates a new instance of OrderListBean
     */
    public OrderListBean() {
        
    }
    
    private String CustomerID="";
    
    public void Search(){
        OrderList = new ArrayList<OrderList>();
        Orders= OrderDAO.select(); // degisecek     arama koyulacak
        for(int i=0;i<Orders.size();i++)
            OrderList.add(new OrderList(Orders.get(i)));
    }
    
  
    
    

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String CustomerID) {
        this.CustomerID = CustomerID;
    }
    
    
    public class OrderList{
        
        OrderList(Order OneOrder){
            this.OneOrder=OneOrder;
            Customer tempcustomer= CustomerDAO.selectOne(OneOrder.CustomerID);
            CustomerName=tempcustomer.Name+" "+tempcustomer.Surname;
            OrderType tempOrderType=OneOrder.getType();
            OrderName=tempOrderType.Description;
            OrderCount=String.valueOf(OneOrder.getCount());
            OrderPrice=String.valueOf(tempOrderType.Prize);
            OrderTotalPrice=String.valueOf(OneOrder.getTotalPrize());
            OrderStats=odemekontrol(BillDAO.selectOne(OneOrder.BillID).isPayed);
        }
        OrderList(Order OneOrder,Customer Customer){ // tek kisilik arama yapildiginda
            this.OneOrder=OneOrder;
            CustomerName=Customer.Name+" "+Customer.Surname;
            OrderType tempOrderType=OneOrder.getType();
            OrderName=tempOrderType.Description;
            OrderCount=String.valueOf(OneOrder.getCount());
            OrderPrice=String.valueOf(tempOrderType.Prize);
            OrderTotalPrice=String.valueOf(OneOrder.getTotalPrize());
            OrderStats=odemekontrol(BillDAO.selectOne(OneOrder.BillID).isPayed);
        }
        
        private Order OneOrder;
        private String CustomerName;
        private String OrderName;
        private String OrderCount;
        private String OrderPrice;
        private String OrderTotalPrice;
        private String OrderStats;

        public Order getOneOrder() {
            return OneOrder;
        }

        public void setOneOrder(Order OneOrder) {
            this.OneOrder = OneOrder;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public String getOrderName() {
            return OrderName;
        }

        public void setOrderName(String OrderName) {
            this.OrderName = OrderName;
        }

        public String getOrderCount() {
            return OrderCount;
        }

        public void setOrderCount(String OrderCount) {
            this.OrderCount = OrderCount;
        }

        public String getOrderPrice() {
            return OrderPrice;
        }

        public void setOrderPrice(String OrderPrice) {
            this.OrderPrice = OrderPrice;
        }

        public String getOrderTotalPrice() {
            return OrderTotalPrice;
        }

        public void setOrderTotalPrice(String OrderTotalPrice) {
            this.OrderTotalPrice = OrderTotalPrice;
        }

        public String getOrderStats() {
            return OrderStats;
        }

        public void setOrderStats(String OrderStats) {
            this.OrderStats = OrderStats;
        }
    }
    
    public String odemekontrol(boolean kontrol){
        if(kontrol)
            return "<span class=\"label label-sm label-success\">Ödendi</span>";
        return "<span class=\"label label-sm label-danger\">Ödenmedi</span>";
    }
}
