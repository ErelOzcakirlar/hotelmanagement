/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import com.DAO.CustomerDAO;
import com.entities.Customer;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author UGUR
 */
@ManagedBean(name="customerara")
@RequestScoped
public class CustomerSearch {
    
    ///ENES TARAFINDAN EKLENDI///
    
    private String cusID;
    private String cusName;
    private String cusSurname;
    private String cusMail;

    public String getCusSurname() {
        return cusSurname;
    }

    public void setCusSurname(String cusSurname) {
        this.cusSurname = cusSurname;
    }

    public String getCusMail() {
        return cusMail;
    }

    public void setCusMail(String cusMail) {
        this.cusMail = cusMail;
    }

    public String getCusPhone() {
        return cusPhone;
    }

    public void setCusPhone(String cusPhone) {
        this.cusPhone = cusPhone;
    }

    private String cusBirth;

    public String getCusBirth() {
        return cusBirth;
    }

    public void setCusBirth(String cusBirth) {
        this.cusBirth = cusBirth;
    }
    private String cusPhone;

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }
     public String getCusID() {
        return cusID;
    }

    public void setCusID(String cusID) {
        this.cusID = cusID;
    }
    private String Lid;

    public String getLid() {
        return Lid;
    }

    public void setLid(String Lid) {
        this.Lid = Lid;
    }
    public Customer obj;
    public void getInfo(ActionEvent e){      
        
        obj = CustomerDAO.selectOne(this.cusID);
        
        this.cusName = obj.Name;
        this.cusSurname = obj.Surname;
        this.cusMail = obj.Mail;
        this.cusPhone = obj.Phone;
        this.cusBirth = obj.BirthDate.toString();
        this.Lid = obj.getLeader().getID();
              
        
    }
    private String saveResult;

    public String getSaveResult() {
        return saveResult;
    }

    public void setSaveResult(String saveResult) {
        this.saveResult = saveResult;
    }
    
    public void saveInfo(ActionEvent e) throws ParseException{        
        
        obj = CustomerDAO.selectOne(this.cusID);
        obj.Name = this.cusName;
        obj.Surname = this.cusSurname;
        obj.Phone = this.cusPhone;
        obj.Mail = this.cusMail;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        obj.BirthDate = formatter.parse(this.cusBirth);
        
        
        Boolean res = CustomerDAO.update(obj);
             
        if(res)
            this.saveResult = "1";
        else
            this.saveResult = "0";
        
        
    }
    
    /////////////////////////////
    
    ArrayList<Customer> list= new ArrayList<Customer>();

    public ArrayList<Customer> getList() {
        return list;
    }

    public void setList(ArrayList<Customer> list) {
        this.list = list;
    }
    String name="";
    String username="";
    String aranacak="";

    public String getAranacak() {
        return aranacak;
    }

    public void setAranacak(String aranacak) {
        this.aranacak = aranacak;
    }
    
    public String getUser(int id){
      return  list.get(id).Name+" "+list.get(id).Surname;
    }
    
    public String getUserID(int id){
      return  list.get(id).getID();
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /**
     This is Request Because it's search :)
     */
    public CustomerSearch() {
        
    }
    
    public void Search(){
        // name i kullanacaz
        if(!aranacak.equals(""))
       list  = CustomerDAO.search(aranacak);
       System.out.println("Kisi sayisi"+list.size());
        // normalde arama fonksyonu koyulacak
    }
    
    public class CustomerObj{
        
    }
    
    
}
