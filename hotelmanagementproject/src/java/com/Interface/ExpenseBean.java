/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import com.DAO.DepartmentDAO;
import com.DAO.ExpenseDAO;
import com.DAO.ReservationDao;
import com.entities.Department;
import com.entities.Expense;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@SessionScoped
public class ExpenseBean {

    String ID;
    String DepartmentId;
    String Department;
    String Description;
    String Price;
    String DateTime;
    
    Expense temp;
    
    ArrayList<Expense> expList = new ArrayList<Expense>();
    
    public ExpenseBean() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
    
    public String getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartmentId(String DepartmentId) {
        this.DepartmentId = DepartmentId;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String Department) {
        this.Department = Department;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String Price) {
        this.Price = Price;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String DateTime) {
        this.DateTime = DateTime;
    }

    public ArrayList<Expense> getExpList() {
        return expList;
    }

    public void setExpList(ArrayList<Expense> expList) {
        this.expList = expList;
    }
    
    public void edit(String id) {
        temp = ExpenseDAO.selectOne(Integer.parseInt(id));
        show();
    }
    
    public void delete(int id) {
         Expense temp2= new Expense(id);
         ExpenseDAO.delete(temp2);
         AllExpenses();
    }

    public void show() {
        if (temp == null) {
            return;
        }
        
        ID = String.valueOf(temp.getID());
        DepartmentId = String.valueOf(temp.DepartmentID);
        Department = DepartmentDAO.getName(temp.DepartmentID);
        Description = temp.Description;
        Price = String.valueOf(temp.Prize);
        DateTime = String.valueOf(temp.DateTime);

    }

    public void save() {
        int tempId;
        try{
            tempId = Integer.parseInt(ID);
        }catch(NumberFormatException nfe){
            tempId = 0;
        }
        temp = new Expense(tempId);
        temp.DepartmentID = Integer.parseInt(DepartmentId);
        temp.Description = Description;
        temp.Prize = new BigDecimal(Price);
        temp.DateTime = new ReservationDao().formatMe(DateTime);
        
        Expense temp2 = ExpenseDAO.selectOne(tempId);
        if(temp2 == null){
            ExpenseDAO.insert(temp);
        }else{
            ExpenseDAO.update(temp);
        }
        AllExpenses();
        ID = "0";
    }
    
    public String allDepartments() {
        ArrayList<Department> list = DepartmentDAO.select();
        String Temp = "[";
        for (int i = 0; i < list.size(); i++) {
            Temp = Temp + "\"" + list.get(i).Description + "    No:" + list.get(i).getID() + "\",";
        }
        Temp = Temp + "\"Yok\"]";
        return Temp;
    }

    public void AllExpenses() {
        expList = ExpenseDAO.select();
    }
    
    public String departmentName(int DepartmentID){
        return DepartmentDAO.getName(DepartmentID);
    }
}
