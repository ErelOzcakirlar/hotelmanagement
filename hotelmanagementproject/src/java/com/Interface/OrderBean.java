/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Interface;

import com.DAO.BillDAO;
import com.DAO.CustomerDAO;
import com.DAO.OrderDAO;
import com.DAO.OrderTypeDAO;
import com.entities.Bill;
import com.entities.Customer;
import com.entities.Human;
import com.entities.Order;
import com.entities.OrderType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author UGUR
 */
@ManagedBean
@SessionScoped
public class OrderBean {

    ArrayList<OrderType> OrderTypes;
    ArrayList<OrderObj> OrderList = new ArrayList<OrderObj>();
    String EmployeeDep = "";
    String CustomerId = "";

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String CustomerId) {
        this.CustomerId = CustomerId;
    }

    public ArrayList<OrderObj> getList() {
        return OrderList;
    }

    // get set
    String count;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    String name = "";

    public OrderBean() {
        // OrderTypes= OrderTypeDAO.select();
    }

    public void addtoOrderList() {
        try {
            OrderObj temp = new OrderObj();
            String tempname = name;
            int TempOrderid = 0;
            if (name.contains(":")) {
                String[] parts = tempname.split(":");
                tempname = parts[parts.length - 1];
                TempOrderid = Integer.valueOf(tempname);
            }
            if (TempOrderid != 0) {
                temp.Type = OrderTypeDAO.selectOne(TempOrderid);
                temp.Count = Integer.valueOf(count);
                OrderList.add(temp);
            }
            // sıfırlıyoruz
            count = "";
            name = "";
        } catch (Exception e) {
        }
        System.out.println("Siparis Sayisi " + name + " " + count);
    }

    public void removeFromOrderList(int x) {
        try {
            OrderList.remove(x);
        } catch (Exception e) {
        }

    }

    public void savetoOrderList() {

        Customer temp = CustomerDAO.selectOne(CustomerId);

        if (temp != null) {
            int tempbillid = -1;
            String tempcustomerid = temp.getID();
            try {

                Customer templeader = temp.getLeader();
                if (templeader != null) {
                    tempcustomerid = templeader.getID();
                }

                tempbillid = BillDAO.selectLastBillID(tempcustomerid);
            } catch (Exception e) {

            }

            if (tempbillid == 0) { // odenmemis fatura yoksa yanlis kisiye cikiyodur
                System.out.println("Odenmemis fatura yok son fatura bulunamadi geri donuldu");
                return;
            }
            for (int i = 0; i < OrderList.size(); i++) {
                Order yeni = new Order();
                yeni.setType(OrderList.get(i).Type);
                yeni.setCount(OrderList.get(i).Count);
                yeni.CustomerID = tempcustomerid;
                yeni.BillID = tempbillid;
                yeni.DateTime = new Date(); // simdiki zamani yaziyoruz 
                OrderDAO.insert(yeni); // eklediii :) 
            }
            cleanOrderList(); // listeyi temizliyoruz
        } else {
            System.out.println("Bu id de musteri yok");
        }

    }

    public void cleanOrderList() {
        OrderList = new ArrayList<OrderObj>();
    }

    public String getAllOrderTypes(String Department) {
        if (EmployeeDep.equals("")) {
            EmployeeDep = Department;
            try {
                int dep = Integer.valueOf(EmployeeDep);
                OrderTypes = OrderTypeDAO.selectByDepartment(dep);
            } catch (Exception e) {
                OrderTypes = OrderTypeDAO.select();
            }
        }
        String Temp = "[";
        for (int i = 0; i < OrderTypes.size(); i++) {
            Temp = Temp + "\"" + OrderTypes.get(i).Description + "    No:" + OrderTypes.get(i).getID() + "\",";
        }
        Temp = Temp + "\"Yok\"]";
        return Temp;
    }

    public class OrderObj {

        OrderType Type;
        int Count = 0;
        String CountStr = "";

        public String getCountStr() {
            CountStr = String.valueOf(Count);
            return CountStr;
        }

        public void setCountStr(String CountStr) {
            this.CountStr = CountStr;
        }
        String name = "";
        String price = "";
        String totalprice = "";

        public String getTotalprice() {
            BigDecimal total = Type.Prize.multiply(new BigDecimal(Count)); // carpma islemi
            totalprice = String.valueOf(total);
            return totalprice;
        }

        public void setTotalprice(String totalprice) {
            this.totalprice = totalprice;
        }

        public String getPrice() {
            this.price = String.valueOf(Type.Prize);
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getName() {
            this.name = Type.Description;
            return name;
        }

        public void setName(String name) {

            this.name = name;
        }

    }
}