/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import com.DAO.RoomDao;
import com.DAO.RoomTypeDao;
import com.entities.Room;
import com.entities.RoomType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author UGUR
 */
@ManagedBean
@RequestScoped
public class RezervationBean {

  
Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
String datein = (String) requestMap.get("datein");
String dateout = (String) requestMap.get("dateout");
String pnumber = (String) requestMap.get("pno");
ArrayList<Room> Rooms = new ArrayList<Room>();
ArrayList<RoomList> RoomLists = new ArrayList<RoomList>();

    public ArrayList<RoomList> getRoomLists() {
        
        return RoomLists;
    }

    public void setRoomLists(ArrayList<RoomList> RoomLists) {
        this.RoomLists = RoomLists;
    }
RoomDao RoomFunc=new RoomDao();
RoomTypeDao RoomTypeFunc=new RoomTypeDao();
    public String getDatein() {
        return datein;
    }

    public void setDatein(String datein) {
        this.datein = datein;
    }

    public String getDateout() {
        return dateout;
    }

    public void setDateout(String dateout) {
        this.dateout = dateout;
    }

    public String getPnumber() {
        return pnumber;
    }

    public void setPnumber(String pnumber) {
        this.pnumber = pnumber;
    }

    public void showRooms(){
        RoomLists = new ArrayList<RoomList>();
        try{
          int number=Integer.valueOf(pnumber);
       Rooms=RoomFunc.showEmptyRoom(number);  
       System.out.println("Dogancan oda sayisi :"+Rooms.size());
       for(int i=0;i<Rooms.size();i++)
           RoomLists.add(new RoomList(Rooms.get(i)));
        }catch(Exception e){}
       System.out.println("Oda list sayisi :"+Rooms.size());
       
    }
    
    
    private Date parseDate(String date, String format) throws ParseException
{
    SimpleDateFormat formatter = new SimpleDateFormat(format);
    return formatter.parse(date);
}
   
    
    
    
    ArrayList<RoomType> getAvailableRoomTypesWithPeopleAndDate(int peopleCount, Date entryDate, Date exitDate){
        RoomDao Roomobj= new RoomDao ();// roomDao object
        RoomTypeDao RoomTypeobj= new RoomTypeDao();  // RoomTypeDao object
        HashMap<Integer, Integer> cache = new HashMap<Integer, Integer>(); // hashmap for roomtype max aviable count
        ArrayList<Room> Rooms=Roomobj.showEmptyRoom(peopleCount); // getting all rom aviable
        System.out.println("Dogancan odalar : "+Rooms.size());
        ArrayList<RoomType> AvailableRoomTypes = new ArrayList<RoomType>();
        // firstly getting all roomtypes
        ArrayList<RoomType> allTypes = RoomTypeobj.select(); // first getting all types for hashmap
        for(int i=0;i<allTypes.size();i++){
            cache.put(allTypes.get(i).getId(), 0);
            
        }
        // added all roomtypes to hashmap and their count 0
        for(int i=0;i<Rooms.size();i++){
           int temptype=Rooms.get(i).getRoomtype_id(); // getting roomtype
           // search type
          int tempcounter= cache.get(temptype);
           tempcounter++;  // increasing 
           cache.put(temptype,tempcounter);
           
        }   
        for(int key : cache.keySet()){
        int biggzero = cache.get(key);
        if(biggzero>0)
          AvailableRoomTypes.add(RoomTypeobj.selectOne(key).get(0)); // get is 0 because the selectone methot return a list why i dont know
}
        return AvailableRoomTypes; 
    }
    
    public class RoomList{ // aynı seyi tabloda da yapabilirim ama fazla sql olur 
        private Room OneRoom;
        private RoomType OneType;
        private String ID;
        private String Name;
        private String EvenCount; // cift
        private String OddCount; // tek
        private String DubleCount; // ranza
        private String TotalCount;
        private String ImageUrl1;
        private String ImageUrl2;
        private String ImageUrl3;
        private String Prize;
        private String RoomNo;
        private String FloorNo;
        
        RoomList(Room newRoom){
            OneRoom=newRoom;
            try{
             ArrayList<RoomType> OneTypes=RoomTypeFunc.selectOne(newRoom.getRoomtype_id()); // doganan düzeltecek bunu
             System.out.println("Secim old mu boyut : aranan oda id si :"+RoomFunc.getRoomtype_id() );
            OneType=OneTypes.get(OneTypes.size()-1);   
            } catch(Exception e){
            System.out.println("Burda mı patladı" );
            }
            ID=String.valueOf(OneRoom.getId());
            Name=OneType.getDescription();
            EvenCount=String.valueOf(OneType.getEvenBed());
            OddCount=String.valueOf(OneType.getOddBed()); 
            DubleCount=String.valueOf(OneType.getBunkBed()); 
            int total=OneType.getEvenBed()*2+OneType.getOddBed()+OneType.getBunkBed()*2;
            TotalCount=String.valueOf(total);
            ImageUrl1=String.valueOf(OneType.getPhoto1url());
            ImageUrl2=String.valueOf(OneType.getPhoto2url());
            ImageUrl3=String.valueOf(OneType.getPhoto3url());
            Prize=String.valueOf(OneType.getPrizePerDay());
            RoomNo=String.valueOf(OneRoom.getRoomNo());
            FloorNo=String.valueOf(OneRoom.getFloorNo());
        }

        public Room getOneRoom() {
            return OneRoom;
        }

        public void setOneRoom(Room OneRoom) {
            this.OneRoom = OneRoom;
        }

        public RoomType getOneType() {
            return OneType;
        }

        public void setOneType(RoomType OneType) {
            this.OneType = OneType;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getEvenCount() {
            return EvenCount;
        }

        public void setEvenCount(String EvenCount) {
            this.EvenCount = EvenCount;
        }

        public String getOddCount() {
            return OddCount;
        }

        public void setOddCount(String OddCount) {
            this.OddCount = OddCount;
        }

        public String getDubleCount() {
            return DubleCount;
        }

        public void setDubleCount(String DubleCount) {
            this.DubleCount = DubleCount;
        }

        public String getImageUrl1() {
            return ImageUrl1;
        }

        public void setImageUrl1(String ImageUrl1) {
            this.ImageUrl1 = ImageUrl1;
        }

        public String getImageUrl2() {
            return ImageUrl2;
        }

        public void setImageUrl2(String ImageUrl2) {
            this.ImageUrl2 = ImageUrl2;
        }

        public String getImageUrl3() {
            return ImageUrl3;
        }

        public void setImageUrl3(String ImageUrl3) {
            this.ImageUrl3 = ImageUrl3;
        }

        public String getPrize() {
            return Prize;
        }

        public void setPrize(String Prize) {
            this.Prize = Prize;
        }

        public String getTotalCount() {
            return TotalCount;
        }

        public void setTotalCount(String TotalCount) {
            this.TotalCount = TotalCount;
        }

        public String getRoomNo() {
            return RoomNo;
        }

        public void setRoomNo(String RoomNo) {
            this.RoomNo = RoomNo;
        }

        public String getFloorNo() {
            return FloorNo;
        }

        public void setFloorNo(String FloorNo) {
            this.FloorNo = FloorNo;
        }
        
       
    }
    
}
