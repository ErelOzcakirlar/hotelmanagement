/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Interface;

import com.DAO.DepartmentDAO;
import com.DAO.EmployeeDAO;
import com.DAO.ReservationDao;
import com.entities.Department;
import com.entities.Employee;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@SessionScoped
public class EmployeeBean {

    String ID;
    String Name;
    String Surname;
    String BirthDate;
    String Phone;
    String DepartmentID;
    String Department;
    String JobDescription;
    String Salary;
    String Password;
    ArrayList<Employee> empList = new ArrayList<Employee>();

    public ArrayList<Employee> getEmpList() {
        return empList;
    }

    public void setEmpList(ArrayList<Employee> empList) {
        this.empList = empList;
    }

    Employee temp;

    public EmployeeBean() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String BirthDate) {
        this.BirthDate = BirthDate;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getDepartmentID() {
        return DepartmentID;
    }

    public void setDepartmentID(String DepartmentID) {
        this.DepartmentID = DepartmentID;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String Department) {
        this.Department = Department;
    }

    public String getJobDescription() {
        return JobDescription;
    }

    public void setJobDescription(String JobDescription) {
        this.JobDescription = JobDescription;
    }

    public String getSalary() {
        return Salary;
    }

    public void setSalary(String Salary) {
        this.Salary = Salary;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public Employee getTemp() {
        return temp;
    }

    public void setTemp(Employee temp) {
        this.temp = temp;
    }

    public String allDepartments() {
        ArrayList<Department> list = DepartmentDAO.select();
        String Temp = "[";
        for (int i = 0; i < list.size(); i++) {
            Temp = Temp + "\"" + list.get(i).Description + "    No:" + list.get(i).getID() + "\",";
        }
        Temp = Temp + "\"Yok\"]";
        return Temp;
    }

    public void edit(String id) {
        temp = EmployeeDAO.selectOne(id);
        show();
    }
    
     public void delete(String id) {
         Employee temp2= new Employee(id,"0");
         EmployeeDAO.delete(temp2);
         AllEmployees();
    }

    public void show() {
        if (temp == null) {
            return;
        }
        ID = temp.getID();
        Name = temp.Name;
        Surname = temp.Surname;
        BirthDate = String.valueOf(temp.BirthDate);
        Phone = temp.Phone;
        DepartmentID = String.valueOf(temp.DepartmentID);
        Department = DepartmentDAO.getName(temp.DepartmentID);
        JobDescription = temp.JobDescription;
        Salary = temp.Salary.toString();
        Password = temp.getPassword();

    }

    public void save() {           
        temp = new Employee(ID, Password);
        temp.Name = Name;
        temp.Surname = Surname;
        temp.BirthDate = new ReservationDao().formatMe(BirthDate);
        temp.Phone = Phone;
        temp.DepartmentID = Integer.parseInt(DepartmentID);        
        temp.JobDescription = JobDescription;        
        temp.Salary = new BigDecimal(Salary);        
        Employee temp2 = EmployeeDAO.selectOne(ID);        
        if(temp2.Name == "Çalışan bulunamadı"){          
            EmployeeDAO.insert(temp);
        }else{
            EmployeeDAO.update(temp);
        }
        AllEmployees();
    }

    public void AllEmployees() {
        empList = EmployeeDAO.select();
    }
    
    public String departmentName(int DepartmentID){
        return DepartmentDAO.getName(DepartmentID);
    }
}
