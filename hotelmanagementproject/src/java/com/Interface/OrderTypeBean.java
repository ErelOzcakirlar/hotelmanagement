/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import com.DAO.DepartmentDAO;
import com.DAO.OrderTypeDAO;
import com.entities.Department;
import com.entities.OrderType;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@SessionScoped
public class OrderTypeBean {

    String ID;
    String DepartmentId;
    String Department;
    String Description;
    String Price;
    String MinPension;
    
    OrderType temp;
    
    ArrayList<OrderType> typeList = new ArrayList<OrderType>();
    
    public OrderTypeBean() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
    
    public String getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartmentId(String DepartmentId) {
        this.DepartmentId = DepartmentId;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String Department) {
        this.Department = Department;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String Price) {
        this.Price = Price;
    }

    public String getMinPension() {
        return MinPension;
    }

    public void setMinPension(String MinPension) {
        this.MinPension = MinPension;
    }

    public ArrayList<OrderType> getTypeList() {
        return typeList;
    }

    public void setExpList(ArrayList<OrderType> typeList) {
        this.typeList = typeList;
    }
    
    public void edit(String id) {
        temp = OrderTypeDAO.selectOne(Integer.parseInt(id));
        show();
    }
    
    public void delete(int id) {
         OrderType temp2= new OrderType(id);
         OrderTypeDAO.delete(temp2);
         AllTypes();
    }

    public void show() {
        if (temp == null) {
            return;
        }
        
        ID = String.valueOf(temp.getID());
        DepartmentId = String.valueOf(temp.DepartmentID);
        Department = DepartmentDAO.getName(temp.DepartmentID);
        Description = temp.Description;
        Price = String.valueOf(temp.Prize);
        MinPension = String.valueOf(temp.minPrizelessPension);

    }

    public void save() {
        int tempId;
        try{
            tempId = Integer.parseInt(ID);
        }catch(NumberFormatException nfe){
            tempId = 0;
        }
        temp = new OrderType(tempId);
        temp.DepartmentID = Integer.parseInt(DepartmentId);
        temp.Description = Description;
        temp.Prize = new BigDecimal(Price);
        temp.minPrizelessPension = Integer.parseInt(MinPension);
        
        OrderType temp2 = OrderTypeDAO.selectOne(tempId);
        if(temp2 == null){
            OrderTypeDAO.insert(temp);
        }else{
            OrderTypeDAO.update(temp);
        }
        ID = "0";
        AllTypes();
        
    }
    
    public String allDepartments() {
        ArrayList<Department> list = DepartmentDAO.select();
        String Temp = "[";
        for (int i = 0; i < list.size(); i++) {
            Temp = Temp + "\"" + list.get(i).Description + "    No:" + list.get(i).getID() + "\",";
        }
        Temp = Temp + "\"Yok\"]";
        return Temp;
    }

    public void AllTypes() {
        typeList = OrderTypeDAO.select();
    }
    
    public String departmentName(int DepartmentID){
        return DepartmentDAO.getName(DepartmentID);
    }
    
}
