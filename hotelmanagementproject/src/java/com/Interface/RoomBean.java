/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import com.DAO.RoomDao;
import com.DAO.RoomTypeDao;
import com.entities.RoomType;
import com.entities.Room;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@SessionScoped
public class RoomBean {
    
    String ID;
    String RoomNo;
    String FloorNo;
    String RoomTypeID;
    String RoomType;
    boolean isReserved;
    boolean isCheckedIn;
    boolean isCheckedOut;
    Date checkInDate;
    Date checkOutDate;
    
    ArrayList<Room> roomList = new ArrayList<Room>();
    
    public ArrayList<Room> getRoomList() {
        return roomList;
    }
    
    public void setRoomList(ArrayList<Room> roomList) {
        this.roomList = roomList;
    }
    
    Room temp;
    
    public RoomBean() {
    }
    
    public String getRoomNo() {
        return RoomNo;
    }
    
    public void setRoomNo(String RoomNo) {
        this.RoomNo = RoomNo;
    }
    
    public String getFloorNo() {
        return FloorNo;
    }
    
    public void setFloorNo(String FloorNo) {
        this.FloorNo = FloorNo;
    }
    
    public String getRoomTypeID() {
        return RoomTypeID;
    }
    
    public void setRoomTypeID(String RoomTypeID) {
        this.RoomTypeID = RoomTypeID;
    }
    
    public String getRoomType() {
        return RoomType;
    }
    
    public void setRoomType(String RoomType) {
        this.RoomType = RoomType;
    }
    
    public Room getTemp() {
        return temp;
    }
    
    public void setTemp(Room temp) {
        this.temp = temp;
    }
    
    public String allRoomTypes() {
        ArrayList<RoomType> list = new RoomTypeDao().select();
        String Temp = "[";
        for (int i = 0; i < list.size(); i++) {
            Temp = Temp + "\"" + list.get(i).getDescription() + "    No:" + list.get(i).getId() + "\",";
        }
        Temp = Temp + "\"Yok\"]";
        return Temp;
    }
    
    public void edit(int id) {
        temp = new RoomDao().selectOne(id).get(0);
        show();
    }
    
    public void delete(int id) {
        new RoomDao().delete(id);
    }
    
    public void show() {
        if (temp == null) {
            return;
        }
        ID = String.valueOf(temp.getId());
        RoomNo = String.valueOf(temp.getRoomNo());
        FloorNo = String.valueOf(temp.getFloorNo());
        RoomTypeID = String.valueOf(temp.getRoomtype_id());
        RoomType = String.valueOf(RoomTypeDao.getName(temp.getRoomtype_id()));
        isReserved = temp.isReserved();
        isCheckedOut = temp.isCheckedOut();
        isCheckedIn = temp.isCheckedIn();
        checkInDate = temp.getCheckInDate();
        checkOutDate = temp.getCheckOutDate();
    }
    
    public void save() {        
        RoomDao manipulator = new RoomDao();
        try{
         manipulator.setId(Integer.parseInt(ID));   
        }catch(Exception e){
        manipulator.setId(0);
          }
        manipulator.setRoomNo(Integer.parseInt(RoomNo));
        manipulator.setFloorNo(Integer.parseInt(FloorNo));
        manipulator.setRoomtype_id(Integer.parseInt(RoomTypeID));
        manipulator.setReserved(isReserved);
        manipulator.setCheckedIn(isCheckedIn);
        manipulator.setCheckedOut(isCheckedOut);
        manipulator.setCheckInDate(checkInDate);
        manipulator.setCheckOutDate(checkInDate);
        Room temp2=null;
        try{
          temp2    = new RoomDao().selectOne(Integer.parseInt(ID)).get(0);
        }
        catch(Exception e){

            System.out.println("Exe girdi");
        }
       
        if (temp2 == null) {
            manipulator.insertNewRoom(RoomNo, FloorNo, RoomTypeID);
        } else {
            manipulator.update();
        }
        ID="0";
        AllRooms();
    }
    
    public void AllRooms() {
        roomList = new RoomDao().select();
    }
    
    public String RoomTypeName(int RoomTypeID) {
        return RoomTypeDao.getName(RoomTypeID);
    }
    
}
