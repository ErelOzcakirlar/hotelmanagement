/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Interface;

import com.DAO.BillDAO;
import com.entities.Bill;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@SessionScoped
public class ReservationLastBean {

       private String leaderId;
       private String totalPrice;
       private String leaderName;
       private String leaderSurName;
       Bill bill = new Bill();
   public void payment(){
       try{
                 int id = BillDAO.selectLastBillID(leaderId);
       bill = BillDAO.selectOne(id);
       leaderName = bill.Leader.Name;
       leaderSurName=bill.Leader.Surname;
       totalPrice = bill.TotalPrize.toString(); 
       } catch(Exception e){}

   }
   public void payMe(){
       bill.pay();
   }

    public String getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(String leaderId) {
        this.leaderId = leaderId;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getLeaderName() {
        return leaderName;
    }

    public void setLeaderName(String leaderName) {
        this.leaderName = leaderName;
    }

    public String getLeaderSurName() {
        return leaderSurName;
    }

    public void setLeaderSurName(String leaderSurName) {
        this.leaderSurName = leaderSurName;
    }
}
