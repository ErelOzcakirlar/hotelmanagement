/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.math.BigDecimal;

/**
 *
 * @author Erel
 */
public class Order extends PaymentRecord {

    public int BillID;
    public String CustomerID;
    private OrderType Type;
    private int Count;
    private BigDecimal TotalPrize;

    public Order() {
        Count = 1;
    }

    public Order(int id) {
        super(id);
        Count = 1;
    }

    public void setType(OrderType type) {
        if (type != null) {
            this.Type = type;
            if (this.Count == 1) {
                this.TotalPrize = type.Prize;
            } else {
                this.TotalPrize = type.Prize.multiply(new BigDecimal(Integer.toString(Count)));
            }
        }
    }

    public OrderType getType() {
        return this.Type;
    }

    public int getCount() {
        return this.Count;
    }

    public void setCount(int count) {
        if (count > 1) {
            this.Count = count;
            if (this.Type != null) {
                this.TotalPrize = this.Type.Prize.multiply(new BigDecimal(Integer.toString(count)));
            }
        }
    }

    public BigDecimal getTotalPrize() {
        return this.TotalPrize;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Order) {
            Order other = (Order) obj;
            if (other.getID() == this.getID()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
