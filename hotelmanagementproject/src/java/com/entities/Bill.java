/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import com.DAO.AgeDiscountDao;
import com.DAO.BillDAO;
import com.DAO.CustomerDAO;
import com.DAO.ReservationDao;
import com.DAO.RoomDao;
import com.DAO.RoomTypeDao;
import com.DAO.PackageDiscountDao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Erel
 */
public class Bill {

    private int ID;
    public int PensionID;
    public Customer Leader;
    public ArrayList<Order> Orders;
    public boolean isPayed;
    public Date PaymentTime;
    public BigDecimal TotalPrize;

    public Bill() {
        isPayed = false;
    }

    public Bill(int id) {
        if (id > 0) {
            this.ID = id;
        }
        isPayed = false;
    }

    public int getID() {
        return this.ID;
    }

    public boolean addOrder(Order value) {
        if (this.Orders == null) {
            this.Orders = new ArrayList<Order>();
        }
        if (this.Orders.contains(value)) {
            return false;
        } else {
            boolean rvalue = this.Orders.add(value);
            return rvalue;
        }
    }

    public boolean removeOrder(Order value) {
        if (this.Orders == null) {
            return false;
        } else {
            if (this.Orders.contains(value)) {
                boolean rvalue = this.Orders.remove(value);
                return rvalue;
            } else {
                return false;
            }
        }
    }

    public void calculateTotalPrize() {

        if (!this.isPayed) {
            BigDecimal sum = BigDecimal.ZERO;

            ReservationDao ReservationGetter = new ReservationDao();
            RoomDao RoomGetter = new RoomDao();
            RoomTypeDao RoomTypeGetter = new RoomTypeDao();
            AgeDiscountDao AgeDiscountGetter = new AgeDiscountDao();
            PackageDiscountDao PackageDiscountGetter = new PackageDiscountDao();

            ArrayList<Reservation> Reservations = ReservationGetter.selectBill(this.ID);

            for (Reservation item : Reservations) {

                Room itemsRoom = RoomGetter.selectOne(item.getRoomId()).get(0);
                RoomType itemsRoomType = RoomTypeGetter.selectOne(itemsRoom.getRoomtype_id()).get(0);
                Customer itemsCustomer = CustomerDAO.selectOne(item.getCustomerId());
                
                int CustomerCount = 0;
                for(Reservation counter: Reservations){
                    if(counter.getRoomId() == itemsRoom.getId()){
                        CustomerCount++;
                    }
                }
                
                Date CurrentDate = new Date();
                
                Date CheckInDate = itemsRoom.getCheckInDate();
                BigDecimal PrizePerDay = itemsRoomType.getPrizePerDay();
                BigDecimal PrizeForCustomer = PrizePerDay.divide(new BigDecimal(Integer.toString(CustomerCount)));
                Long DayDifference = (CurrentDate.getTime() - CheckInDate.getTime()) / (1000 * 60 * 60 * 24);
                
                BigDecimal DiscountPercent;

                ArrayList<PackageDiscount> PackageDiscounts = PackageDiscountGetter.selectRoomTypeId(itemsRoomType.getId());
                BigDecimal PackageDiscountPercent = new BigDecimal('0');
                boolean PackageDiscountValid = false;

                for (PackageDiscount PD : PackageDiscounts) {
                    // Gün sayısı uyuşuyorsa ve daha yüksek bir indirim geçerli değilse
                    if (PD.getDaycount() == DayDifference && PD.getPercent().compareTo(PackageDiscountPercent) == 1) {
                        PackageDiscountPercent = PD.getPercent();
                    }
                }

                if (!PackageDiscountValid) {//Paket indirimi yapılmadıysa yaşa bak

                    ArrayList<AgeDiscount> AgeDiscounts = AgeDiscountGetter.selectRoomTypeId(itemsRoomType.getId());
                    int CustomersAge = (int)((CurrentDate.getTime() - itemsCustomer.BirthDate.getTime()) / (1000 * 60 * 60 * 24 * 365.25));
                    BigDecimal AgeDiscountPercent = new BigDecimal('0');

                    for (AgeDiscount AD : AgeDiscounts) {
                        // Müşteri uygun yaş aralığındaysa ve daha yüksek bir indirim yapılmadıysa
                        if (AD.getMinAge() <= CustomersAge && AD.getMaxAge() >= CustomersAge && AD.getPercent().compareTo(AgeDiscountPercent) == 1) {
                            AgeDiscountPercent = AD.getPercent();
                        }
                    }
                    DiscountPercent = AgeDiscountPercent;
                }
                else
                {
                    DiscountPercent = PackageDiscountPercent;
                }

                //Prize = PrizePerDay * ( Now - CheckInDate )
                BigDecimal Prize = PrizeForCustomer.multiply(new BigDecimal(DayDifference.toString()));
                //Prize -= Prize * Discount ??
                Prize = Prize.subtract(Prize.multiply(DiscountPercent));
                sum = sum.add(Prize);
            }

            if (this.Orders != null) {
                for (Order o : this.Orders) {
                    if (o.getType().minPrizelessPension <= this.PensionID) {
                        sum = sum.add(o.getTotalPrize());
                    }
                }
            }
            this.TotalPrize = sum.abs();
        }
    }

    public void pay() {
        if(!isPayed){
            this.isPayed = true;
            this.PaymentTime = new Date();
            BillDAO.update(this);
            new RoomDao().RoomCheckOut(ID);
        }
    }
}
