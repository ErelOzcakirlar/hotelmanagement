package com.entities;

import java.math.BigDecimal;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class PackageDiscount {
	  private int id;
	  private int daycount;
	  private int nightcount;
	  private int roomtype_id;
	  private BigDecimal percent;
	  private boolean isActive;
	   
  public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDaycount() {
		return daycount;
	}
	public void setDaycount(int daycount) {
		this.daycount = daycount;
	}
	public int getNightcount() {
		return nightcount;
	}
	public void setNightcount(int nightcount) {
		this.nightcount = nightcount;
	}
	public int getRoomtype_id() {
		return roomtype_id;
	}
	public void setRoomtype_id(int roomtype_id) {
		this.roomtype_id = roomtype_id;
	}
	public BigDecimal getPercent() {
		return percent;
	}
	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
}
