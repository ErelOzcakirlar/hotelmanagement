/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.entities;

import com.DAO.CustomerDAO;

/**
 *
 * @author Erel
 */
public class Customer extends Human{
    private String LeaderID;
    public String Mail;
    
    public Customer clone(){
        Customer cloned = new Customer(this.getID(),this.LeaderID);
        cloned.Name = this.Name;
        cloned.Surname = this.Surname;
        cloned.Mail = this.Mail;
        cloned.BirthDate = this.BirthDate;
        return cloned;
    }
    
    public Customer(){
        
    }
    
    public Customer(String id,String leaderID){
        super(id);
        if( checkID(leaderID) ){
            this.LeaderID = leaderID;
        }
    }
    
    public Customer (String id,String LeaderId,int u){
        super(id);
        this.LeaderID=LeaderId;
    }
    
    public Customer getLeader(){
        if(LeaderID.equals(this.getID())){
            return this.clone();
        }else{
            return CustomerDAO.selectOne(LeaderID);
        }
    }
}
