package com.entities;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.DAO.ConnectorManager;

@ManagedBean(name = "roomtype")
@SessionScoped
public class RoomType {

    private int id;
    private int evenBed;
    private int oddBed;
    private int bunkBed;
    private int bedCount;
    private BigDecimal prizePerDay;
    private String description;
    private String photo1url;
    private String photo2url;
    private String photo3url;

    public BigDecimal getPrizePerDay() {
        return prizePerDay;
    }

    public void setPrizePerDay(BigDecimal prizePerDay) {
        this.prizePerDay = prizePerDay;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEvenBed() {
        return evenBed;
    }

    public void setEvenBed(int evenBed) {
        this.evenBed = evenBed;
    }

    public int getOddBed() {
        return oddBed;
    }

    public void setOddBed(int oddBed) {
        this.oddBed = oddBed;
    }

    public int getBunkBed() {
        return bunkBed;
    }

    public void setBunkBed(int bunkBed) {
        this.bunkBed = bunkBed;
    }

    public int getBedCount() {
        return bedCount;
    }

    public void setBedCount(int bedCount) {
        this.bedCount = bedCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto1url() {
        return photo1url;
    }

    public void setPhoto1url(String photo1url) {
        this.photo1url = photo1url;
    }

    public String getPhoto2url() {
        return photo2url;
    }

    public void setPhoto2url(String photo2url) {
        this.photo2url = photo2url;
    }

    public String getPhoto3url() {
        return photo3url;
    }

    public void setPhoto3url(String photo3url) {
        this.photo3url = photo3url;
    }

    public void delete(int id) {
        PreparedStatement ps = null;
        Connection con = null;
        if (id != 0) {
            try {
                con = ConnectorManager.getConnection();
                String sql = "DELETE FROM roomtype WHERE id=" + id;
                ps = con.prepareStatement(sql);
                int i = ps.executeUpdate();
                if (i > 0) {
                    System.out.println("Row deleted successfully");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    con.close();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
