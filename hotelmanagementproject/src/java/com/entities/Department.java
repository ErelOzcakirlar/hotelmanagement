/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.entities;

import java.util.ArrayList;

/**
 *
 * @author Erel
 */
public class Department {
    private int ID;
    public String Name;
    public String Description;
    public int EmployeeCount;
    public ArrayList<Employee> Employees;
    
    public Department(){
        
    }
    
    public Department(int id){
        if(id > 0){
            this.ID = id;
        }
    }
    
    public int getID(){
        return this.ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getEmployeeCount() {
        return EmployeeCount;
    }

    public void setEmployeeCount(int EmployeeCount) {
        this.EmployeeCount = EmployeeCount;
    }
    
    public boolean addEmployee( Employee value ){
        if(this.Employees == null){
            this.Employees = new ArrayList<Employee>();
        }
        if(this.Employees.contains(value)){
            return false;
        }else{
            boolean rvalue = this.Employees.add(value);
            this.EmployeeCount = Employees.size();
            return rvalue;
        }
    }
    
    public boolean removeEmployee( Employee value ){
        if(this.Employees == null){
            return false;
        }else{
            if(this.Employees.contains(value)){
                boolean rvalue = this.Employees.remove(value);
                this.EmployeeCount = Employees.size();
                return rvalue;
            }else{
                return false;
            }
        }
    }
}
