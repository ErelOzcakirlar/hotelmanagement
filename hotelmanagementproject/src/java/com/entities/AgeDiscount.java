package com.entities;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.DAO.ConnectorManager;
@ManagedBean(name="agediscount")
@SessionScoped
public class AgeDiscount {
	
	  private int id;
	   private int roomtype_id;
	   private int minAge;
	   private int maxAge;
	   private BigDecimal percent;
	   private boolean isActive;
   
   public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRoomtype_id() {
		return roomtype_id;
	}
	public void setRoomtype_id(int roomtype_id) {
		this.roomtype_id = roomtype_id;
	}
	public int getMinAge() {
		return minAge;
	}
	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}
	public int getMaxAge() {
		return maxAge;
	}
	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}
	public BigDecimal getPercent() {
		return percent;
	}
	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	 
	public void delete(int id){
		PreparedStatement ps = null;
		Connection con = null;
		if(id !=0)
		{
		     try
		         {
		        con = ConnectorManager.getConnection();
		        String sql = "DELETE FROM agediscount WHERE id="+id;
		        ps= con.prepareStatement(sql); 
		        int i = ps.executeUpdate();
		         if(i >0){ 
		          System.out.println("Row deleted successfully");
		         }
		       }
		        catch(Exception e)
		          {
		           e.printStackTrace();
		          }
		         finally
		               {
		             try{
		          con.close();
		          ps.close();		
		         }catch(Exception e)
		{
		e.printStackTrace();
		  }
		 }
	   }
     } 
     
   
}
