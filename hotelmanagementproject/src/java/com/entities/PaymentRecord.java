/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.entities;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Erel
 */
public class PaymentRecord {
    private int ID;
    public Date DateTime;
    
    
    public PaymentRecord(){
        
    }
    
    public PaymentRecord(int id){
        if(id > 0){
            this.ID = id;
        }
    }
    
    public int getID(){
        return this.ID;
    }
}
