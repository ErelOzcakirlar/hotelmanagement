/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import com.DAO.EmployeeDAO;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Erel
 */
public class Employee extends Human {

    public int DepartmentID;
    public String JobDescription;
    public BigDecimal Salary;
    private String Password;

    public Employee() {

    }

    public Employee(String ID, String password) {
        super(ID);
        this.Password = password;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public Date getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(Date BirthDate) {
        this.BirthDate = BirthDate;
    }

    public String getPassword() {
        return this.Password;
    }

    public void setPassword(String pass) {
        this.Password = pass;
    }

    public int getDepartmentID() {
        return DepartmentID;
    }

    public void setDepartmentID(int DepartmentID) {
        this.DepartmentID = DepartmentID;
    }

    public String getJobDescription() {
        return JobDescription;
    }

    public void setJobDescription(String JobDescription) {
        this.JobDescription = JobDescription;
    }

    public BigDecimal getSalary() {
        return Salary;
    }

    public void setSalary(BigDecimal Salary) {
        this.Salary = Salary;
    }

    public static boolean login(String id, String password) {
        boolean value;
        if (checkID(id)) {
            password = password.replace("'", "");
            password = password.replace("-", "");
            if (EmployeeDAO.login(id, password)) {
                value = true; // logined
            } else {
                value = false; // not logined
                //message wrong userID or password
            }
        } else {
            value = false; // not logined
            //message invalid userID
        }
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Employee) {
            Employee other = (Employee) obj;
            if (other.getID().contentEquals(this.getID())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
