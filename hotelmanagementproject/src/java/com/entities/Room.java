package com.entities;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "room")
@SessionScoped
public class Room {

    private int id;
    private int roomtype_id;
    private boolean isReserved;
    private boolean isCheckedIn;
    private boolean isCheckedOut;
    private Date checkInDate;
    private Date checkOutDate;
    private int evenBedCount;
    private int oddBedCount;
    private int bunkBedCount;
    private int roomNo;
    private int floorNo;
    
    private static int OnePersonCount;
    private static int TwoPersonCount;
    private static int ThreePersonCount;
    private static int FourPersonCount;
    private static int FivePersonCount;
    private static int SixPersonCount;
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoomtype_id() {
        return roomtype_id;
    }

    public void setRoomtype_id(int roomtype_id) {
        this.roomtype_id = roomtype_id;
    }

    public boolean isCheckedIn() {
        return isCheckedIn;
    }

    public void setCheckedIn(boolean isCheckedIn) {
        this.isCheckedIn = isCheckedIn;
    }

    public boolean isCheckedOut() {
        return isCheckedOut;
    }

    public void setCheckedOut(boolean isCheckedOut) {
        this.isCheckedOut = isCheckedOut;
    }

    public Date getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    public Date getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public boolean isReserved() {
        return isReserved;
    }

    public void setReserved(boolean isReserved) {
        this.isReserved = isReserved;
    }

    public int getEvenBedCount() {
        return evenBedCount;
    }

    public void setEvenBedCount(int evenBedCount) {
        this.evenBedCount = evenBedCount;
    }

    public int getOddBedCount() {
        return oddBedCount;
    }

    public void setOddBedCount(int oddBedCount) {
        this.oddBedCount = oddBedCount;
    }

    public int getBunkBedCount() {
        return bunkBedCount;
    }

    public void setBunkBedCount(int bunkBedCount) {
        this.bunkBedCount = bunkBedCount;
    }

    public int getOnePersonCount() {
        return OnePersonCount;
    }

    public void setOnePersonCount(int OnePersonCount) {
        this.OnePersonCount = OnePersonCount;
    }

    public int getTwoPersonCount() {
        return TwoPersonCount;
    }

    public void setTwoPersonCount(int TwoPersonCount) {
        this.TwoPersonCount = TwoPersonCount;
    }

    public int getThreePersonCount() {
        return ThreePersonCount;
    }

    public void setThreePersonCount(int ThreePersonCount) {
        this.ThreePersonCount = ThreePersonCount;
    }

    public int getFourPersonCount() {
        return FourPersonCount;
    }

    public void setFourPersonCount(int FourPersonCount) {
        this.FourPersonCount = FourPersonCount;
    }

    public int getFivePersonCount() {
        return FivePersonCount;
    }

    public void setFivePersonCount(int FivePersonCount) {
        this.FivePersonCount = FivePersonCount;
    }

    public int getSixPersonCount() {
        return SixPersonCount;
    }

    public void setSixPersonCount(int SixPersonCount) {
        this.SixPersonCount = SixPersonCount;
    }

    public int getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(int roomNo) {
        this.roomNo = roomNo;
    }

    public int getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(int floorNo) {
        this.floorNo = floorNo;
    }

}
