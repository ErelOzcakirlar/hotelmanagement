/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.entities;

import java.util.Date;
import java.lang.Character;

/**
 *
 * @author Erel
 */
public abstract class Human {
    private String ID;
    public String Name;
    public String Surname;
    public String Phone;
    public Date BirthDate;
    
    public Human(){
        
    }
    
    public Human(String id){
        if(checkID(id)){
            this.ID = id;
        }
    }
    
    public String getID(){
        return this.ID;
    }
    
    public static boolean checkID(String id){
        boolean value = true;
        if( id.length() == 11 ){
            for(int i=0; i < 11; i++ ){
                char c = id.charAt(i);
                if(!Character.isLetterOrDigit(c) || Character.isLowerCase(c)){
                    value = false;
                    break;
                }
            }
        }else{
            value = false;
        }
        return value;
    }
    
}
