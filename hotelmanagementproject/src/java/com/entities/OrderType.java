/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.entities;

import java.math.BigDecimal;

/**
 *
 * @author Erel
 */
public class OrderType {
    private int ID;
    public int DepartmentID;
    public String Description;
    public BigDecimal Prize;
    public int minPrizelessPension;
    
    public OrderType(){
        
    }
    
    public OrderType(int id){
        if( id > 0 ){
            this.ID = id;
        }
    }
    
    public int getID(){
        return this.ID;
    }

    public int getDepartmentID() {
        return DepartmentID;
    }

    public void setDepartmentID(int DepartmentID) {
        this.DepartmentID = DepartmentID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public BigDecimal getPrize() {
        return Prize;
    }

    public void setPrize(BigDecimal Prize) {
        this.Prize = Prize;
    }

    public int getMinPrizelessPension() {
        return minPrizelessPension;
    }

    public void setMinPrizelessPension(int minPrizelessPension) {
        this.minPrizelessPension = minPrizelessPension;
    }
    
    
}
