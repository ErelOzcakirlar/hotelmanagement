/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.entities;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Erel
 */
public class Expense extends PaymentRecord{
    public int DepartmentID;
    public String Description;
    public BigDecimal Prize;
    
    public Expense(){
        
    }
    public Expense(int id){
        super(id);
    }

    public int getDepartmentID() {
        return DepartmentID;
    }

    public void setDepartmentID(int DepartmentID) {
        this.DepartmentID = DepartmentID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public BigDecimal getPrize() {
        return Prize;
    }

    public void setPrize(BigDecimal Prize) {
        this.Prize = Prize;
    }

    public Date getDateTime() {
        return DateTime;
    }

    public void setDateTime(Date DateTime) {
        this.DateTime = DateTime;
    }
    
}
