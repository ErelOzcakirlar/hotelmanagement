/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.DAO;

import com.entities.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@ApplicationScoped
public class EmployeeDAO {

    /**
     * Creates a new instance of EmployeeDAO
     */
    public EmployeeDAO() {
    }
    public static Employee selectOne(String id) {
        Employee value;
        if (Employee.checkID(id)) {
                try {
                    Connection conn = ConnectorManager.getConnection();
                    PreparedStatement stm = conn.prepareStatement("select * from employee where id = " + id);
                    ResultSet set = stm.executeQuery();
                    if (set.next()) {
                        value = new Employee(set.getString(1), set.getString(2));
                        value.Name = set.getString(3);
                        value.Surname = set.getString(4);
                        value.BirthDate = set.getDate(5);
                        value.Phone = set.getString(6);
                        value.DepartmentID = set.getInt(7);
                        value.JobDescription = set.getString(8);
                        value.Salary = set.getBigDecimal(9);
                    } else {
                        value = new Employee();
                        value.Name = "Çalışan bulunamadı";
                    }
                    set.close();
                    stm.close();
                    conn.close();
                } catch (SQLException se) {
                    System.out.println("SQLException:" + se.getMessage());
                    value = null;
                }
        } else {
            value = new Employee();
            value.Name = "Geçersiz ID";
        }
        return value;
    }

    public static ArrayList<Employee> select() {
        ArrayList<Employee> value = new ArrayList<Employee>();       
            try {
                Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("select id from employee");
                ResultSet set = stm.executeQuery();
                ArrayList<String> IDs = new ArrayList<String>();
                while (set.next()) {
                    IDs.add(set.getString(1));
                }
                set.close();
                stm.close();
                conn.close();
                for(String s:IDs){
                    value.add(selectOne(s));
                }
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
            }
        return value;
    }
    
    public static boolean insert(Employee e){
        boolean value = true;
            try {
                Connection conn = ConnectorManager.getConnection();
                SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                PreparedStatement stm = conn.prepareStatement("insert employee values('" + 
                        e.getID() + "','" +
                        e.getPassword() + "','" +
                        e.Name + "','" +
                        e.Surname + "','" +
                        fmt.format(e.BirthDate) + "','" + 
                        e.Phone + "','" +
                        e.DepartmentID + "','" +
                        e.JobDescription + "','" + 
                        e.Salary + "')");
                int rowCount = stm.executeUpdate();
                if( rowCount == 0){
                    value = false;
                }
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
                value = false;
            }
        return value;
    }
    
    public static boolean update(Employee e){
        boolean value = true;
            try {
                Connection conn = ConnectorManager.getConnection();
                SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                PreparedStatement stm = conn.prepareStatement("update employee set " +
                        "name='" + e.Name + "'," +
                        "surname='" + e.Surname + "'," +
                        "phone='" + e.Phone + "'," +
                        "birthDate='" + fmt.format(e.BirthDate) + "'," +
                        "departmentId=" + e.DepartmentID + "," +
                        "jobDescription='" + e.JobDescription + "'," +
                        "salary=" + e.Salary.toString() + "," +
                        "password='" + e.getPassword() + "' where id='" + e.getID() + "'");
                int rowCount = stm.executeUpdate();
                if( rowCount == 0){
                    value = false;
                }
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
                value = false;
            }
        return value;
    }
    
    public static boolean delete(Employee e){
        boolean value = true;
            try {
                Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("delete from employee where id=" + e.getID());
                int rowCount = stm.executeUpdate();
                if( rowCount == 0){
                    value = false;
                }
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
                value = false;
            }
        return value;
    }
    
    public static boolean login(String id,String password){
        boolean value = false;
            try {
               Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("select password from employee where id=" + id);
                ResultSet set = stm.executeQuery();
                if( set.next() ){
                    if( password.contentEquals(set.getString(1))){
                        value = true;
                    }
                }
                set.close();
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
            }
        return value;
    }
}
