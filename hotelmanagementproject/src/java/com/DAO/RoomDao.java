package com.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.entities.Room;
import com.entities.RoomType;
import java.text.DateFormat;
import java.text.Format;

@ManagedBean(name = "roomdao")
@SessionScoped
public class RoomDao {

    private int id;
    private int roomtype_id;
    private boolean isReserved;
    private boolean isCheckedIn;
    private boolean isCheckedOut;
    private Date checkInDate;
    private Date checkOutDate;
    private int roomNo;
    private int floorNo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoomtype_id() {
        return roomtype_id;
    }

    public void setRoomtype_id(int roomtype_id) {
        this.roomtype_id = roomtype_id;
    }

    public boolean isCheckedIn() {
        return isCheckedIn;
    }

    public void setCheckedIn(boolean isCheckedIn) {
        this.isCheckedIn = isCheckedIn;
    }

    public boolean isCheckedOut() {
        return isCheckedOut;
    }

    public void setCheckedOut(boolean isCheckedOut) {
        this.isCheckedOut = isCheckedOut;
    }

    public Date getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    public Date getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public boolean isReserved() {
        return isReserved;
    }

    public int getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(int roomNo) {
        this.roomNo = roomNo;
    }

    public int getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(int floorNo) {
        this.floorNo = floorNo;
    }

    public void setReserved(boolean isReserved) {
        this.isReserved = isReserved;
    }

    public ArrayList<Room> select() {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<Room> list = new ArrayList<Room>();

        String sql = "select * from room";

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                Room type = new Room();
                type.setId(result.getInt("id"));
                type.setRoomtype_id(result.getInt("typeId"));
                type.setReserved(result.getBoolean("isReserved"));
                type.setCheckedIn(result.getBoolean("isCheckedIn"));
                type.setCheckedOut(result.getBoolean("isCheckedOut"));
                type.setCheckInDate(result.getDate("checkInDate"));
                type.setCheckOutDate(result.getDate("checkOutDate"));
                type.setRoomNo(result.getInt("roomNo"));
                type.setFloorNo(result.getInt("floorNo"));
                list.add(type);
               
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<Room> selectOne(int id) {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<Room> list = new ArrayList<Room>();

        String sql = "select * from room WHERE id=" + id;

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                Room type = new Room();
                type.setId(result.getInt("id"));
                type.setRoomtype_id(result.getInt("typeId"));
                type.setReserved(result.getBoolean("isReserved"));
                type.setCheckedIn(result.getBoolean("isCheckedIn"));
                type.setCheckedOut(result.getBoolean("isCheckedOut"));
                try {
                    type.setCheckInDate(result.getDate("checkInDate"));
                } catch (SQLException se) {
                    type.setCheckInDate(null);
                }
                try {
                    type.setCheckOutDate(result.getDate("checkOutDate"));
                } catch (SQLException se) {
                    type.setCheckOutDate(null);
                }
                type.setRoomNo(result.getInt("roomNo"));
                type.setFloorNo(result.getInt("floorNo"));
                list.add(type);
                /*
                 Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
                 List<Room> checkedItems = new ArrayList<Room>();
                 for (Room item : list) {
                 if (checked.get(item.getId())!= null) {
                 checkedItems.add(item);
                 //type.delete(type.getId());
                 }	
                 }
                 */

            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public void insert() {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        int i = 0;
   
            PreparedStatement ps = null;
            Connection con = null;
            try {
                con = ConnectorManager.getConnection();
                String sql = "INSERT INTO room(typeId, isReserved, isCheckedIn,isCheckedOut,checkInDate,checkOutDate,roomNo,floorNo) VALUES('"+roomtype_id+"',"+isReserved+","+isCheckedIn+","+isCheckedOut+"',"+checkInDate+","+checkOutDate+","+roomNo+"',"+floorNo+")";
                ps = con.prepareStatement(sql); 

                i = ps.executeUpdate();
                System.out.println("Data Added Successfully");
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    con.close();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
    }
    
    public void insertNewRoom(String roomno,String floorno,String type){
        String command="insert room values('0','type','0','0','0','1970-01-01 00:00:00','1970-01-01 00:00:00','roomno','floorno' )";
        command=command.replace("type", type);
         command= command.replace("roomno", roomno);
          command=command.replace("floorno", floorno);
            try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement(command);
            stm.executeUpdate();
            stm.close();
            conn.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        } 
    }

    public String update() {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        int i = 0;

        if (id != 0) {
            PreparedStatement ps = null;
            Connection con = null;
            try {
                con = ConnectorManager.getConnection();
                ps = con.prepareStatement("UPDATE room SET " +
                "typeId='" + roomtype_id + "'," +
                "isReserved='" + (isReserved ? "1" : "0") + "'," +
                "isCheckedIn='" + (isCheckedIn ? "1" : "0") + "'," +
                "isCheckedOut='" + (isCheckedOut ? "1" : "0") + "'," +
                "checkInDate='" + fmt.format(checkInDate) + "'," +
                "checkOutDate='" + fmt.format(checkOutDate) + "' WHERE id=" + id); 
                
                i = ps.executeUpdate();
                System.out.println("Data Added Successfully");
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    con.close();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (i > 0) {
                return "index";
            } else {
                return "invalid";
            }
        } else {
            return "invalid";
        }
    }

    
    public String RoomCheckIn(int id, Date time1, Date time2) {
        Connection dbConnection = null;
        Statement statement = null;

        try {
            System.out.println("ah girdi try a roomcheckinde");
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String zaman1 = dateFormat.format(time1);
            String zaman2 = dateFormat.format(time2);

            String updateTableSQL = "UPDATE room SET isReserved=1 , isCheckedIn=1 , checkInDate='" + zaman1 + "', checkOutDate='" + zaman2 + "' WHERE id =" + id;

            dbConnection = ConnectorManager.getConnection();
            statement = dbConnection.createStatement();

            System.out.println(updateTableSQL);

            // execute update SQL stetement
            statement.execute(updateTableSQL);

            System.out.println("Record is updated to room table!");
            statement.close();
            dbConnection.close();

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return "index";
        }

    }
    
    public static String getNumbers(int RoomId) {
        String value;
        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("select roomNo,floorNo from room where id = " + RoomId);
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = "Oda No:" + set.getString(1) + "<br/>Kat No:" + set.getString(2);
            } else {
                set.close();
                stm.close();
                conn.close();
                value = "";
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = "";
        }
        return value;
    }

    public void RoomCheckOut(int BillId) {
        Connection con = ConnectorManager.getConnection();
        try {
            PreparedStatement state = con.prepareStatement("SELECT roomId FROM reservation JOIN bill ON reservation.billId=bill.Id WHERE bill.id=" + BillId + " GROUP BY roomId");
            ResultSet set = state.executeQuery();
            ArrayList<Integer> IDs = new ArrayList<Integer>();
            while (set.next()) {
                IDs.add(set.getInt(1));
            }
            set.close();
            state.close();
            
            Date Now = new Date();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            for (int i : IDs) {
                state = con.prepareStatement("UPDATE room SET isReserved=0,isCheckedIn=0,isCheckedOut=1,CheckInDate='1970-01-01 00:00:00',CheckOutDate='" + fmt.format(Now) + "' WHERE id=" + i);
                state.execute();
                state.close();
            }
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException: " + se.getMessage());
        }

    }

    public ArrayList<Room> showEmptyRoom(int peopleCount) {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<Room> list = new ArrayList<Room>();

        String sql = "select room.id,room.typeId,room.isReserved,room.isCheckedIn,room.isCheckedOut,room.checkInDate,room.checkOutDate,room.roomNo,room.floorNo,roomtype.evenBed,roomtype.oddBed,roomtype.bunkBed from room join roomtype on room.typeId = roomtype.id WHERE room.isReserved = false";

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                Room type = new Room();
                type.setId(result.getInt("id"));
                type.setRoomtype_id(result.getInt("typeId"));
                type.setReserved(result.getBoolean("isReserved"));
                type.setCheckedIn(result.getBoolean("isCheckedIn"));
                type.setCheckedOut(result.getBoolean("isCheckedOut"));
                type.setEvenBedCount(result.getInt("evenBed"));
                type.setOddBedCount(result.getInt("oddBed"));
                type.setBunkBedCount(result.getInt("bunkBed"));
                type.setRoomNo(result.getInt("roomNo"));
                type.setFloorNo(result.getInt("floorNo"));

                int evenBedCount = result.getInt("evenBed");
                int oddBedCount = result.getInt("oddBed");
                int bunkBedCount = result.getInt("bunkBed");
                int totalPeople = evenBedCount * 2 + oddBedCount + bunkBedCount * 2;

                if (peopleCount <= totalPeople) {
                    list.add(type);
                }
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<Room> showEmptyRoomCount(int peopleCount) {
        int onePersonCount = 0;
        int twoPersonCount = 0;
        int threePersonCount = 0;
        int fourPersonCount = 0;
        int fivePersonCount = 0;
        int sixPersonCount = 0;
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<Room> list = new ArrayList<Room>();

        String sql = "select room.id,room.typeId,room.isReserved,room.isCheckedIn,room.isCheckedOut,room.checkInDate,room.checkOutDate,room.roomNo,room.floorNo,roomtype.evenBed,roomtype.oddBed,roomtype.bunkBed from room join roomtype on room.typeId = roomtype.id";

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                Room type = new Room();
                type.setId(result.getInt("id"));
                type.setRoomtype_id(result.getInt("typeId"));
                type.setReserved(result.getBoolean("isReserved"));
                type.setCheckedIn(result.getBoolean("isCheckedIn"));
                type.setCheckedOut(result.getBoolean("isCheckedOut"));
                type.setEvenBedCount(result.getInt("evenBed"));
                type.setOddBedCount(result.getInt("oddBed"));
                type.setBunkBedCount(result.getInt("bunkBed"));
                type.setRoomNo(result.getInt("roomNo"));
                type.setFloorNo(result.getInt("floorNo"));

                int evenBedCount = result.getInt("evenBed");
                int oddBedCount = result.getInt("oddBed");
                int bunkBedCount = result.getInt("bunkBed");
                int totalPeople = evenBedCount * 2 + oddBedCount + bunkBedCount * 2;
                if (!result.getBoolean("isReserved")) {
                    if (totalPeople <= peopleCount) {
                        if (totalPeople == 1) {
                            onePersonCount++;
                            type.setOnePersonCount(onePersonCount);
                        }
                        if (totalPeople == 2) {
                            twoPersonCount++;
                            type.setTwoPersonCount(twoPersonCount);
                        }

                        if (totalPeople == 3) {
                            threePersonCount++;
                            type.setThreePersonCount(threePersonCount);
                        }

                        if (totalPeople == 4) {
                            fourPersonCount++;
                            type.setFourPersonCount(fourPersonCount);
                        }

                        if (totalPeople == 5) {
                            fivePersonCount++;
                            type.setFivePersonCount(fivePersonCount);
                        }

                        if (totalPeople == 6) {
                            sixPersonCount++;
                            type.setSixPersonCount(sixPersonCount);
                        }
                        list.add(type);
                    }
                }
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public int showEmptyRoomWithRoomtypeId(int roomtypeId, Date entryDate, Date exitDate) {
        int emptyRoomCount = 0;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        PreparedStatement state = null;
        ResultSet result = null;

        String sql = "select * from room";

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {

                String checkInDate = dateFormat.format(result.getDate("checkInDate"));
                String checkOutDate = dateFormat.format(result.getDate("checkOutDate"));
                String yourEntryDate = dateFormat.format(entryDate);
                String yourExitDate = dateFormat.format(exitDate);

                String time[] = checkInDate.split("-", 3); // Bu dizi o an odadaki giris tarihini tutar
                int day1 = Integer.parseInt(time[2]); //Gun 
                int month1 = Integer.parseInt(time[1]); //Ay
                int year1 = Integer.parseInt(time[0]); //Yil

                String time2[] = checkInDate.split("-", 3);// Bu dizi o an odadaki cikis tarihini tutar
                int day2 = Integer.parseInt(time2[2]);//Gun 
                int month2 = Integer.parseInt(time2[1]);//Ay
                int year2 = Integer.parseInt(time2[0]);//Yil

                String time3[] = checkInDate.split("-", 3);// Senin istedigin giris tarihi
                int day3 = Integer.parseInt(time3[2]);//Gun 
                int month3 = Integer.parseInt(time3[1]);//Ay
                int year3 = Integer.parseInt(time3[0]);//Yil

                String time4[] = checkInDate.split("-", 3);// Senin istedigin cikis tarihi
                int day4 = Integer.parseInt(time4[2]);//Gun 
                int month4 = Integer.parseInt(time4[1]);//Ay
                int year4 = Integer.parseInt(time4[0]);//Yil

                if (result.getInt("typeId") == roomtypeId) {
                    if (month3 == month2 && year3 == year2 && day3 > day2) {
                        emptyRoomCount++;
                    } else if (month4 == month1 && year3 == year1 && day4 < day1) {
                        emptyRoomCount++;
                    } else if (month3 > month2 && year3 == year2) {
                        emptyRoomCount++;
                    } else if (month4 < month1 && year3 == year1) {
                        emptyRoomCount++;
                    }
                }
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return emptyRoomCount;
    }

    public void delete(int id) {
        ArrayList<Room> list = selectOne(id);
        if (list.size() > 0) {
            Room temp = list.get(0);
            if (!temp.isReserved()) {
                try {
                    Connection conn = ConnectorManager.getConnection();
                    PreparedStatement stm = conn.prepareStatement("delete from room where id=" + id);
                    stm.executeUpdate();
                    stm.close();
                    conn.close();
                } catch (SQLException se) {
                    System.out.println("SQLException:" + se.getMessage());
                }
            }
        }
    }
}
