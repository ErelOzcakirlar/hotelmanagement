package com.DAO;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.entities.AgeDiscount;
import com.entities.PackageDiscount;

@ManagedBean(name = "packagediscountdao")
@SessionScoped
public class PackageDiscountDao {

    private int id;
    private int daycount;
    private int nightcount;
    private int roomtype_id;
    private BigDecimal percent;
    private boolean isActive;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDaycount() {
        return daycount;
    }

    public void setDaycount(int daycount) {
        this.daycount = daycount;
    }

    public int getNightcount() {
        return nightcount;
    }

    public void setNightcount(int nightcount) {
        this.nightcount = nightcount;
    }

    public int getRoomtype_id() {
        return roomtype_id;
    }

    public void setRoomtype_id(int roomtype_id) {
        this.roomtype_id = roomtype_id;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public ArrayList<PackageDiscount> select() {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<PackageDiscount> list = new ArrayList<PackageDiscount>();

        String sql = "select * from packagediscount";

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                PackageDiscount type = new PackageDiscount();
                type.setId(result.getInt("id"));
                type.setDaycount(result.getInt("dayCount"));
                type.setNightcount(result.getInt("nightCount"));
                type.setRoomtype_id(result.getInt("roomType"));
                type.setPercent(result.getBigDecimal("percent"));
                type.setActive(result.getBoolean("isActive"));
                list.add(type);
                /*
                 Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
                 List<PackageDiscount> checkedItems = new ArrayList<PackageDiscount>();
                 for (PackageDiscount item : list) {
                 if (checked.get(item.getId())!= null) {
                 checkedItems.add(item);
                 type.delete(type.getId());
				
                 }
                 }
                 */
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<PackageDiscount> selectOne(int id) {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<PackageDiscount> list = new ArrayList<PackageDiscount>();

        String sql = "select * from packagediscount WHERE id=" + id;

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                PackageDiscount type = new PackageDiscount();
                type.setId(result.getInt("id"));
                type.setDaycount(result.getInt("dayCount"));
                type.setNightcount(result.getInt("nightCount"));
                type.setRoomtype_id(result.getInt("roomType"));
                type.setPercent(result.getBigDecimal("percent"));
                type.setActive(result.getBoolean("isActive"));
                list.add(type);
                /*
                 Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
                 List<PackageDiscount> checkedItems = new ArrayList<PackageDiscount>();
                 for (PackageDiscount item : list) {
                 if (checked.get(item.getId())!= null) {
                 checkedItems.add(item);
                 type.delete(type.getId());
                 }
                 }
                 */
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<PackageDiscount> selectRoomTypeId(int roomtypeid) {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<PackageDiscount> list = new ArrayList<PackageDiscount>();

        String sql = "select * from packagediscount WHERE id=" + roomtypeid;

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                PackageDiscount type = new PackageDiscount();
                type.setId(result.getInt("id"));
                type.setDaycount(result.getInt("dayCount"));
                type.setNightcount(result.getInt("nightCount"));
                type.setRoomtype_id(result.getInt("roomType"));
                type.setPercent(result.getBigDecimal("percent"));
                type.setActive(result.getBoolean("isActive"));
                if (result.getBoolean("isActive")) {
                    list.add(type);
                }
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public String insert() {
        int i = 0;

        if (id != 0) {
            PreparedStatement ps = null;
            Connection con = null;
            try {
                con = ConnectorManager.getConnection();
                String sql = "INSERT INTO packagediscount(id, roomType, dayCount, nightCount,percent,isActive) VALUES(?,?,?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                ps.setInt(2, daycount);
                ps.setInt(3, nightcount);
                ps.setInt(4, roomtype_id);
                ps.setBigDecimal(5, percent);
                ps.setBoolean(6, isActive);

                i = ps.executeUpdate();
                System.out.println("Data Added Successfully");
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    con.close();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (i > 0) {
                return "index";
            } else {
                return "invalid";
            }
        } else {
            return "invalid";
        }
    }

    public String update(int percent) {
        Connection dbConnection = null;
        Statement statement = null;

        String updateTableSQL = "UPDATE packagediscount"
                + " SET percent =" + percent
                + " WHERE id = 1";

        try {
            dbConnection = ConnectorManager.getConnection();
            statement = dbConnection.createStatement();

            System.out.println(updateTableSQL);

            // execute update SQL stetement
            statement.execute(updateTableSQL);

            System.out.println("Record is updated to packagediscount table!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return "index";
        }

    }
}
