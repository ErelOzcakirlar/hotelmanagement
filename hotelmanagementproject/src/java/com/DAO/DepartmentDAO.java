/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.DAO;

import com.entities.Department;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@ApplicationScoped
public class DepartmentDAO {

    /**
     * Creates a new instance of DepartmentDAO
     */
    public DepartmentDAO() {
    }

    public static Department selectOne(int id) {
        Department value;

        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("select * from department where id = " + id);
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = new Department(set.getInt(1));
                value.Name = set.getString(2);
                value.Description = set.getString(3);
                set.close();
                stm.close();
                stm = conn.prepareStatement("select id from employee where departmentId=" + id);
                set = stm.executeQuery();
                ArrayList<String> IDs = new ArrayList<String>();
                while (set.next()) {
                    IDs.add(set.getString(1));
                }
                set.close();
                stm.close();
                conn.close();
                for (String s : IDs) {
                    value.addEmployee(EmployeeDAO.selectOne(s));
                }
            } else {
                set.close();
                stm.close();
                conn.close();
                value = null;
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = null;
        }
        return value;
    }

    public static ArrayList<Department> select() {
        ArrayList<Department> value = new ArrayList<Department>();
        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("select id from department");
            ResultSet set = stm.executeQuery();
            ArrayList<Integer> IDs = new ArrayList<Integer>();
            while (set.next()) {
                IDs.add(set.getInt(1));
            }
            set.close();
            stm.close();
            conn.close();
            for (int i : IDs) {
                value.add(selectOne(i));
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return value;
    }

    public static String getName(int departmentId) {
        String value;
        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("select name from department where id = " + departmentId);
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = set.getString(1);
            } else {
                set.close();
                stm.close();
                conn.close();
                value = null;
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = null;
        }
        return value;
    }

    public static boolean insert(Department b) {
        boolean value = true;
        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("insert department values("
                    + b.getID() + ",'"
                    + b.Name + "','"
                    + b.Description + "',"
                    + b.EmployeeCount + ")");
            int rowCount = stm.executeUpdate();
            if (rowCount == 0) {
                value = false;
            }
            stm.close();
            conn.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = false;
        }

        return value;
    }

    public static boolean update(Department b) {
        boolean value = true;

        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("update customer set("
                    + "name='" + b.Name + "',"
                    + "description=" + b.Description + "',"
                    + "employeeCount=" + b.EmployeeCount + ") where id='" + b.getID() + "'");
            int rowCount = stm.executeUpdate();
            if (rowCount == 0) {
                value = false;
            }
            stm.close();
            conn.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = false;
        }
        return value;
    }
    
    public static boolean delete(Department d){
        boolean value = true;
            try {
                Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("delete from department where id=" + d.getID());
                int rowCount = stm.executeUpdate();
                if( rowCount == 0){
                    value = false;
                }
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
                value = false;
            }
        return value;
    }
}
