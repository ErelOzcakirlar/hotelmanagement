/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.DAO;

import com.entities.Bill;
import com.entities.Order;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@ApplicationScoped
public class BillDAO {

    /**
     * Creates a new instance of BillDAO
     */
    public BillDAO() {
    }

    public static Bill selectOne(int id) {
        Bill value;
        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("select * from bill where id=" + id);
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = new Bill(set.getInt(1));
                value.PensionID = set.getInt(2);
                String LeaderID = set.getString(3);
                value.isPayed = set.getBoolean(4);
                try {
                    value.PaymentTime = set.getDate(5);
                } catch (SQLException se) {
                    value.PaymentTime = null;
                }

                BigDecimal Total = set.getBigDecimal(6);
                set.close();
                stm.close();
                conn.close();
                ArrayList<Order> Orders = OrderDAO.selectByBill(id);
                for (Order o : Orders) {
                    value.addOrder(o);
                }
                value.Leader = CustomerDAO.selectOne(LeaderID);
                if (value.isPayed) {
                    value.TotalPrize = Total;
                } else {
                    value.calculateTotalPrize();//Burda hata var
                }
            } else {
                set.close();
                stm.close();
                conn.close();
                value = null;
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = null;
        }
        return value;
    }

    public static ArrayList<Bill> select() {
        ArrayList<Bill> value = new ArrayList<Bill>();

        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("select id from bill");
            ResultSet set = stm.executeQuery();
            ArrayList<Integer> IDs = new ArrayList<Integer>();
            while (set.next()) {
                IDs.add(set.getInt(1));
            }
            set.close();
            stm.close();
            conn.close();
            for (int i : IDs) {
                value.add(selectOne(i));
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return value;
    }

    public static int selectLastBillID(String LeaderId) {
        int value=0;
        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("select id from bill where leaderId='" + LeaderId + "' and isPayed=0");
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = set.getInt(1);
            }
            set.close();
            stm.close();
            conn.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return value;
    }

    public static boolean insert(Bill b) {
        boolean value = true;
        try {
            Connection conn = ConnectorManager.getConnection();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String paymentTime = dateFormat.format(b.PaymentTime);
            PreparedStatement stm = conn.prepareStatement("insert bill values("
                    + b.getID() + ","
                    + b.PensionID + ",'"
                    + b.Leader.getID() + "',"
                    + (b.isPayed ? "1" : "0") + ",'"
                    + paymentTime + "',"
                    + b.TotalPrize + ")");
            int rowCount = stm.executeUpdate();
            if (rowCount == 0) {
                value = false;
            }
            stm.close();
            conn.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = false;
        }
        return value;
    }

    public static boolean update(Bill b) {
        boolean value = true;
        try {
            Connection conn = ConnectorManager.getConnection();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String paymentTime = dateFormat.format(b.PaymentTime);
            PreparedStatement stm = conn.prepareStatement("update bill set "
                    + "pensionID=" + b.PensionID + ","
                    + "leaderId='" + b.Leader.getID() + "',"
                    + "isPayed=" + (b.isPayed ? "1" : "0") + ","
                    + "paymentTime='" + paymentTime + "',"
                    + "totalPrize=" + b.TotalPrize + " where id=" + b.getID());
            int rowCount = stm.executeUpdate();
            if (rowCount == 0) {
                value = false;
            }
            stm.close();
            conn.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = false;
        }
        return value;
    }
}
