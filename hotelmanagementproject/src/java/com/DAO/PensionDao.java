package com.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.entities.AgeDiscount;
import com.entities.PackageDiscount;
import com.entities.Pension;

public class PensionDao {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public static String getNameById(int id){
        String value="";
        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("select name from pension where id = " + id);
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = set.getString(1);
            } else {
                set.close();
                stm.close();
                conn.close();
                value = null;
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = null;
        }
        return value;
    }

    public ArrayList<Pension> select() {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<Pension> list = new ArrayList<Pension>();

        String sql = "select * from pension";

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                Pension type = new Pension();
                type.setId(result.getInt("id"));
                type.setName(result.getString("name"));
                list.add(type);
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Pension> selectOne(int id) {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<Pension> list = new ArrayList<Pension>();

        String sql = "select * from pension WHERE id=" + id;

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                Pension type = new Pension();
                type.setId(result.getInt("id"));
                type.setName(result.getString("name"));
                list.add(type);

            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public String insert() {
        int i = 0;

        if (id != 0) {
            PreparedStatement ps = null;
            Connection con = null;
            try {
                con = ConnectorManager.getConnection();
                String sql = "INSERT INTO pension(id, name) VALUES(?,?)";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                ps.setString(2, name);

                i = ps.executeUpdate();
                System.out.println("Data Added Successfully");
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    con.close();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (i > 0) {
                return "index";
            } else {
                return "invalid";
            }
        } else {
            return "invalid";
        }
    }
}
