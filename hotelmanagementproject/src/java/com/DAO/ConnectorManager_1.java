package com.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ConnectorManager_1 {
    public static Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://85.95.253.126:1453/hotelmanagement", "uzak", "12345608");
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return conn;
    }
    public static Date convertToDate(String stringFormat){
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date value = null;
        try{
            value = format.parse(stringFormat);
        }catch(ParseException pe){
            System.out.println("Date parsing exception:"+pe.getMessage());
        }
        return value;
    }
}