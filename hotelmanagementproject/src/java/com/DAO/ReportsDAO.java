/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.DAO;

import com.entities.Room;
import com.entities.Customer;
import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@ApplicationScoped
public class ReportsDAO {

    /**
     * Creates a new instance of ReportsDAO
     */
    public ReportsDAO() {

    }

    public String occupancyRate() {
        String value = "";
        int ReservedRooms = 0, AllRooms = 0;
        Connection con = ConnectorManager.getConnection();
        try {
            PreparedStatement stm = con.prepareStatement("SELECT COUNT(*) FROM room");
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                AllRooms = set.getInt(1);
            }
            set.close();
            stm.close();

            stm = con.prepareStatement("SELECT COUNT(*) FROM room WHERE isReserved=1");
            set = stm.executeQuery();
            if (set.next()) {
                ReservedRooms = set.getInt(1);
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }

        value += ReservedRooms + " / " + AllRooms;
        return value;
    }

    public ArrayList<Customer> CurrentCustomers() {
        ArrayList<Customer> value = new ArrayList<Customer>();
        Connection con = ConnectorManager.getConnection();
        try {
            PreparedStatement stm = con.prepareStatement("SELECT customerId FROM reservation JOIN bill ON reservation.billId=bill.id WHERE isPayed=0");
            ResultSet set = stm.executeQuery();
            while (set.next()) {
                value.add(CustomerDAO.selectOne(set.getString(1)));
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return value;
    }

    public String CurrentCustomerNames() {
        String value = "";
        Connection con = ConnectorManager.getConnection();
        try {
            PreparedStatement stm = con.prepareStatement("SELECT customer.name,customer.surname FROM reservation JOIN bill ON reservation.billId=bill.id JOIN customer ON reservation.customerId=customer.id WHERE isPayed=0");
            ResultSet set = stm.executeQuery();
            while (set.next()) {
                value += set.getString(1) + " " + set.getString(2) + "<br/>";
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return value;
    }

    public ArrayList<Customer> CurrentCustomersByRoom(int roomId) {
        ArrayList<Customer> value = new ArrayList<Customer>();
        Connection con = ConnectorManager.getConnection();
        try {
            PreparedStatement stm = con.prepareStatement("SELECT customerId FROM reservation JOIN bill ON reservation.billId=bill.id WHERE isPayed=0 AND roomId=" + roomId);
            ResultSet set = stm.executeQuery();
            while (set.next()) {
                value.add(CustomerDAO.selectOne(set.getString(1)));
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return value;
    }

    public String dailyProfit() {
        int price = 0;
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<Integer> list = new ArrayList<Integer>();

        String sql = "select roomtype.prizePerDay from room join roomtype on room.typeId = roomtype.id where room.isReserved = 1 ";
        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                price = price + (result.getInt("prizePerDay"));
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return String.valueOf(price) + " TL ";
    }

    public String currentCustomersCount() {
        ArrayList<Customer> value = new ArrayList<Customer>();
        Connection con = ConnectorManager.getConnection();
        try {
            PreparedStatement stm = con.prepareStatement("SELECT customerId FROM reservation JOIN bill ON reservation.billId=bill.id WHERE isPayed=0");
            ResultSet set = stm.executeQuery();
            while (set.next()) {
                value.add(CustomerDAO.selectOne(set.getString(1)));
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return String.valueOf(value.size());
    }

    public String mostChoiceRoom() {
        int max = 0;
        int returnId = 0;
        String desc = null;
        PreparedStatement state = null;
        ResultSet result = null;

        String sql = "SELECT roomId,COUNT(roomId) FROM reservation GROUP BY roomId";
        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                if (result.getInt(2) > max) {
                    max = result.getInt(2);
                    returnId = result.getInt(1);
                }
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String sql2 = "SELECT description FROM roomtype JOIN room ON roomtype.id = room.typeId WHERE room.id = " + returnId;
        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql2);
            result = state.executeQuery();
            while (result.next()) {
                desc = result.getString("description");
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return desc;
    }

    public BigDecimal SumOfBills(Date FirstDate, Date LastDate) {
        BigDecimal value = new BigDecimal('0');
        Connection con = ConnectorManager.getConnection();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String first = dateFormat.format(FirstDate);
        String last = dateFormat.format(LastDate);
        try {
            PreparedStatement stm = con.prepareStatement("SELECT SUM(totalPrize) FROM bill WHERE paymentTime BETWEEN '" + first + "' AND '" + last + "'");
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = set.getBigDecimal(1);
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return value;
    }

    public BigDecimal SumOfEmployeeSalaries() {
        BigDecimal value = new BigDecimal('0');
        Connection con = ConnectorManager.getConnection();
        try {
            PreparedStatement stm = con.prepareStatement("SELECT SUM(Salary) FROM employee");
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = set.getBigDecimal(1);
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return value;
    }

    public String SumOfEmployeeSalariesString() {
        return String.valueOf(SumOfEmployeeSalaries()) + " TL";
    }

    public String SumOfBillsToday() {
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date yesterday = cal.getTime();
        String value = String.valueOf(SumOfBills(yesterday, today));
        if(value.equals("null")){
            return "0 TL";
        }else{
            return  value + " TL";
        }
    }

    public String SumOfSalariesByDepartment(int departmentId) {
        BigDecimal value = new BigDecimal('0');
        Connection con = ConnectorManager.getConnection();
        try {
            PreparedStatement stm = con.prepareStatement("SELECT SUM(Salary) FROM employee WHERE departmentId=" + departmentId);
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = set.getBigDecimal(1);
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return String.valueOf(value);
    }

    public BigDecimal SumOfExpenses(Date FirstDate, Date LastDate) {
        BigDecimal value = new BigDecimal('0');
        Connection con = ConnectorManager.getConnection();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String first = dateFormat.format(FirstDate);
        String last = dateFormat.format(LastDate);
        try {
            PreparedStatement stm = con.prepareStatement("SELECT SUM(prize) FROM expense WHERE paymentTime BETWEEN '" + first + "' AND '" + last + "'");
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = set.getBigDecimal(1);
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return value;
    }

    public String SumOfExpensesToday() {
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date yesterday = cal.getTime();
        BigDecimal value = SumOfExpenses(yesterday, today);
        if (value == null) {
            return "0 TL";
        } else {
            return String.valueOf(value) + " TL";
        }

    }

    public BigDecimal SumOfExpensesByDepartment(int departmentId, Date FirstDate, Date LastDate) {
        BigDecimal value = new BigDecimal('0');
        Connection con = ConnectorManager.getConnection();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String first = dateFormat.format(FirstDate);
        String last = dateFormat.format(LastDate);
        try {
            PreparedStatement stm = con.prepareStatement("SELECT SUM(prize) FROM bill WHERE departmentId=" + departmentId + " AND paymentTime BETWEEN '" + first + "' AND '" + last + "'");
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = set.getBigDecimal(1);
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return value;
    }

    public String dailyOrderMoney(Date today, Date tomorrow) {
        BigDecimal value = new BigDecimal('0');
        Connection con = ConnectorManager.getConnection();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String first = dateFormat.format(today);
        String last = dateFormat.format(tomorrow);
        try {
            PreparedStatement stm = con.prepareStatement("SELECT SUM(totalPrize) FROM orders WHERE dateTime BETWEEN '" + first + "' AND '" + last + "'");
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = set.getBigDecimal(1);
            }
            set.close();
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }
        return String.valueOf(value);
    }

    public String dailyOrderMoneyToday() {

        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, +1);
        Date tomorrow = cal.getTime();
        String value = dailyOrderMoney(today, tomorrow);
        System.out.println(value);
        if (value.equals("null")) {
            return "0 TL";
        } else {
            return String.valueOf(value + " TL");
        }
    }
}
