/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.DAO;

import com.entities.Order;
import com.entities.OrderType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@ApplicationScoped
public class OrderDAO {

    /**
     * Creates a new instance of OrderDAO
     */
    public OrderDAO() {
    }

    public static Order selectOne(int id) {
        Order value = null;
        System.out.println("Order:"+id+"icin selectOne çalışıyor");
        try {
            Connection con = ConnectorManager.getConnection();
            PreparedStatement stm = con.prepareStatement("select * from orders join ordertype on orders.typeID=ordertype.id where orders.id=" + id);
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = new Order(set.getInt(1));
                OrderType typeValue = new OrderType(set.getInt(2));
                typeValue.DepartmentID = set.getInt(9);
                typeValue.Description = set.getString(10);
                typeValue.Prize = set.getBigDecimal(11);
                typeValue.minPrizelessPension = set.getInt(12);
                value.CustomerID = set.getString(3);
                value.BillID = set.getInt(4);
                try{
                    value.DateTime = set.getDate(5);
                }catch(SQLException se){
                    value.DateTime = null;
                }
                
                int count = set.getInt(6);
                
                set.close();
                stm.close();
                con.close();
                
                value.setType(typeValue);
                value.setCount(count);
                
            } else {
                set.close();
                stm.close();
                con.close();
            }

        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }

        return value;
    }

    public static ArrayList<Order> select() {
        ArrayList<Order> value = new ArrayList<Order>();

        try {
            Connection con = ConnectorManager.getConnection();
            PreparedStatement stm = con.prepareStatement("select id from orders");
            ResultSet set = stm.executeQuery();
            ArrayList<Integer> IDs = new ArrayList<Integer>();
            while (set.next()) {
                IDs.add(set.getInt(1));
            }
            set.close();
            stm.close();
            con.close();
            for (int i : IDs) {
                value.add(selectOne(i));
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }

        return value;
    }
    
    public ArrayList<Order> selectByDate(Date FirstDate, Date LastDate){
        ArrayList<Order> value = new ArrayList<Order>();
        
        try {
            Connection con = ConnectorManager.getConnection();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String first = dateFormat.format(FirstDate);
            String last = dateFormat.format(LastDate);
            PreparedStatement stm = con.prepareStatement("select id from orders where dateTime between '" + first + "' and '" + last + "'");
            ResultSet set = stm.executeQuery();
            ArrayList<Integer> IDs = new ArrayList<Integer>();
            while (set.next()) {
                IDs.add(set.getInt(1));
            }
            set.close();
            stm.close();
            con.close();
            for (int i : IDs) {
                value.add(selectOne(i));
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }

        return value;
    }

    public static ArrayList<Order> selectByBill(int billId) {
        ArrayList<Order> value = new ArrayList<Order>();

        try {
            Connection con = ConnectorManager.getConnection();
            PreparedStatement stm = con.prepareStatement("select id from orders where billId="+billId);
            ResultSet set = stm.executeQuery();
            ArrayList<Integer> IDs = new ArrayList<Integer>();
            while (set.next()) {
                IDs.add(set.getInt(1));
            }
            set.close();
            stm.close();
            con.close();
            for (int i : IDs) {
                value.add(selectOne(i));
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }

        return value;
    }
    
    public static boolean insert(Order o) {
        boolean value = true;

        try {
            Connection con = ConnectorManager.getConnection();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateTime = dateFormat.format(o.DateTime);
            PreparedStatement stm = con.prepareStatement("insert orders values("
                    + o.getID() + ","
                    + o.getType().getID() + ",'"
                    + o.CustomerID + "',"
                    + o.BillID + ",'"
                    + dateTime + "',"
                    + o.getCount() + ","
                    + o.getTotalPrize() + ")");
            int rowCount = stm.executeUpdate();
            if (rowCount == 0) {
                value = false;
            }
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = false;
        }

        return value;
    }
    
    public static boolean delete(Order o){
        boolean value = true;
            try {
                Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("delete from orders where id=" + o.getID());
                int rowCount = stm.executeUpdate();
                if( rowCount == 0){
                    value = false;
                }
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
                value = false;
            }
        return value;
    }
}
