/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.DAO;

import com.entities.Expense;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@ApplicationScoped
public class ExpenseDAO {

    /**
     * Creates a new instance of ExpenseDAO
     */
    public ExpenseDAO() {
    }
    public static Expense selectOne(int id) {
        Expense value = null;
            try {
                Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("select * from expense where id = " + id);
                ResultSet set = stm.executeQuery();
                if (set.next()) {
                    value = new Expense(set.getInt(1));
                    value.Description = set.getString(2);
                    value.Prize = set.getBigDecimal(3);
                    value.DateTime = set.getDate(4);
                    value.DepartmentID = set.getInt(5);
                }
                set.close();
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
            }
        return value;
    }

    public static ArrayList<Expense> select() {
        ArrayList<Expense> value = new ArrayList<Expense>();
            try {
               Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("select id from expense");
                ResultSet set = stm.executeQuery();
                ArrayList<Integer> IDs = new ArrayList<Integer>();
                while (set.next()) {
                    IDs.add(set.getInt(1));
                }
                set.close();
                stm.close();
                conn.close();
                for (int i : IDs) {
                    value.add(selectOne(i));
                }
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
            }
        return value;
    }

    public static boolean insert(Expense e) {
        boolean value = true;
            try {
                Connection conn = ConnectorManager.getConnection();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String dateTime = dateFormat.format(e.DateTime);
                PreparedStatement stm = conn.prepareStatement("insert expense values("
                        + e.getID() + ",'"
                        + e.Description + "',"
                        + e.Prize + ",'"
                        + dateTime + "',"
                        + e.DepartmentID + ")");
                int rowCount = stm.executeUpdate();
                if (rowCount == 0) {
                    value = false;
                }
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
                value = false;
            }
        return value;
    }
    
    public static boolean update(Expense e) {
        boolean value = true;

        try {
            Connection con = ConnectorManager.getConnection();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateTime = dateFormat.format(e.DateTime);
            PreparedStatement stm = con.prepareStatement("update expense set "
                    + "departmentId=" + e.DepartmentID + ","
                    + "description='" + e.Description + "',"
                    + "prize=" + e.Prize + ","
                    + "paymentTime='" + dateTime + "' where id=" + e.getID());
            int rowCount = stm.executeUpdate();
            if (rowCount == 0) {
                value = false;
            }
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = false;
        }

        return value;
    }
    
    public static boolean delete(Expense e){
        boolean value = true;
            try {
                Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("delete from expense where id=" + e.getID());
                int rowCount = stm.executeUpdate();
                if( rowCount == 0){
                    value = false;
                }
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
                value = false;
            }
        return value;
    }
}
