/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.DAO;

import com.entities.OrderType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author Erel
 */
@ManagedBean
@ApplicationScoped
public class OrderTypeDAO {

    /**
     * Creates a new instance of OrderTypeDAO
     */
    public OrderTypeDAO() {
    }

    public static OrderType selectOne(int id) {
        OrderType value;

        try {
            Connection con = ConnectorManager.getConnection();
            PreparedStatement stm = con.prepareStatement("select * from ordertype where id = " + id);
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = new OrderType(set.getInt(1));
                value.DepartmentID = set.getInt(2);
                value.Description = set.getString(3);
                value.Prize = set.getBigDecimal(4);
                value.minPrizelessPension = set.getInt(5);
                set.close();
                stm.close();
                con.close();

            } else {
                set.close();
                stm.close();
                con.close();
                value = null;
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = null;
        }

        return value;
    }

    public static ArrayList<OrderType> select() {
        ArrayList<OrderType> value = new ArrayList<OrderType>();

        try {
            Connection con = ConnectorManager.getConnection();
            PreparedStatement stm = con.prepareStatement("select id from ordertype");
            ResultSet set = stm.executeQuery();
            ArrayList<Integer> IDs = new ArrayList<Integer>();
            while (set.next()) {
                IDs.add(set.getInt(1));
            }
            set.close();
            stm.close();
            con.close();
            for (int i : IDs) {
                value.add(selectOne(i));
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }

        return value;
    }
    
    public static ArrayList<OrderType> selectByDepartment(int departmentId){
        ArrayList<OrderType> value = new ArrayList<OrderType>();
        
        try {
            Connection con = ConnectorManager.getConnection();
            PreparedStatement stm = con.prepareStatement("select id from ordertype where departmentId=" + departmentId);
            ResultSet set = stm.executeQuery();
            ArrayList<Integer> IDs = new ArrayList<Integer>();
            while (set.next()) {
                IDs.add(set.getInt(1));
            }
            set.close();
            stm.close();
            con.close();
            for (int i : IDs) {
                value.add(selectOne(i));
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
        }

        return value;
    }

    public static boolean insert(OrderType b) {
        boolean value = true;

        try {
            Connection con = ConnectorManager.getConnection();
            PreparedStatement stm = con.prepareStatement("insert ordertype values("
                    + b.getID() + ","
                    + b.DepartmentID + ",'"
                    + b.Description + "',"
                    + b.Prize + ","
                    + b.minPrizelessPension + ")");
            int rowCount = stm.executeUpdate();
            if (rowCount == 0) {
                value = false;
            }
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = false;
        }

        return value;
    }

    public static boolean update(OrderType b) {
        boolean value = true;

        try {
            Connection con = ConnectorManager.getConnection();
            PreparedStatement stm = con.prepareStatement("update ordertype set "
                    + "departmentId=" + b.DepartmentID + ","
                    + "description='" + b.Description + "',"
                    + "prize=" + b.Prize + ","
                    + "minimumPrizelessPensionType=" + b.minPrizelessPension + " where id=" + b.getID());
            int rowCount = stm.executeUpdate();
            if (rowCount == 0) {
                value = false;
            }
            stm.close();
            con.close();
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = false;
        }

        return value;
    }
    
    public static boolean delete(OrderType o){
        boolean value = true;
            try {
                Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("delete from ordertype where id=" + o.getID());
                int rowCount = stm.executeUpdate();
                if( rowCount == 0){
                    value = false;
                }
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
                value = false;
            }
        return value;
    }
}
