package com.DAO;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.entities.AgeDiscount;
import com.entities.RoomType;

@ManagedBean(name = "agediscountdao")
@SessionScoped
public class AgeDiscountDao {

    private int id;
    private int roomtype_id;
    private int minAge;
    private int maxAge;
    private BigDecimal percent;
    private boolean isActive;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoomtype_id() {
        return roomtype_id;
    }

    public void setRoomtype_id(int roomtype_id) {
        this.roomtype_id = roomtype_id;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public ArrayList<AgeDiscount> select() {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<AgeDiscount> list = new ArrayList<AgeDiscount>();

        String sql = "select * from agediscount";

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                AgeDiscount type = new AgeDiscount();
                type.setId(result.getInt("id"));
                type.setRoomtype_id(result.getInt("roomType"));
                type.setMinAge(result.getInt("minAge"));
                type.setMaxAge(result.getInt("maxAge"));
                type.setPercent(result.getBigDecimal("percent"));
                type.setActive(result.getBoolean("isActive"));
                list.add(type);

                /*Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
                List<AgeDiscount> checkedItems = new ArrayList<AgeDiscount>();
                for (AgeDiscount item : list) {
                    if (checked.get(item.getId()) != null) {
                        checkedItems.add(item);
                        type.delete(type.getId());
                    }
                }*/
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<AgeDiscount> selectOne(int id) {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<AgeDiscount> list = new ArrayList<AgeDiscount>();

        String sql = "select * from agediscount WHERE id=" + id;

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                AgeDiscount type = new AgeDiscount();
                type.setId(result.getInt("id"));
                type.setRoomtype_id(result.getInt("roomType"));
                type.setMinAge(result.getInt("minAge"));
                type.setMaxAge(result.getInt("maxAge"));
                type.setPercent(result.getBigDecimal("percent"));
                type.setActive(result.getBoolean("isActive"));
                list.add(type);

                /*Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
                List<AgeDiscount> checkedItems = new ArrayList<AgeDiscount>();
                for (AgeDiscount item : list) {
                    if (checked.get(item.getId()) != null) {
                        checkedItems.add(item);
                        type.delete(type.getId());
                    }
                }*/
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<AgeDiscount> selectRoomTypeId(int roomtypeid) {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<AgeDiscount> list = new ArrayList<AgeDiscount>();

        String sql = "select * from agediscount WHERE id=" + roomtypeid;

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                AgeDiscount type = new AgeDiscount();
                type.setId(result.getInt("id"));
                type.setRoomtype_id(result.getInt("roomType"));
                type.setMinAge(result.getInt("minAge"));
                type.setMaxAge(result.getInt("maxAge"));
                type.setPercent(result.getBigDecimal("percent"));
                type.setActive(result.getBoolean("isActive"));
                if (result.getBoolean("isActive")) {
                    list.add(type);
                }
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public String insert() {
        int i = 0;

        if (id != 0) {
            PreparedStatement ps = null;
            Connection con = null;
            try {
                con = ConnectorManager.getConnection();
                String sql = "INSERT INTO agediscount(id, roomType, minAge, maxAge,percent,isActive) VALUES(?,?,?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                ps.setInt(2, roomtype_id);
                ps.setInt(3, minAge);
                ps.setInt(4, maxAge);
                ps.setBigDecimal(5, percent);
                ps.setBoolean(6, isActive);

                i = ps.executeUpdate();
                System.out.println("Data Added Successfully");
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    con.close();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (i > 0) {
                return "index";
            } else {
                return "invalid";
            }
        } else {
            return "invalid";
        }
    }

    public String update(int percent) {
        Connection dbConnection = null;
        Statement statement = null;

        String updateTableSQL = "UPDATE agediscount"
                + " SET percent =" + percent
                + " WHERE id = 1";

        try {
            dbConnection = ConnectorManager.getConnection();
            statement = dbConnection.createStatement();

            System.out.println(updateTableSQL);

            // execute update SQL stetement
            statement.execute(updateTableSQL);

            System.out.println("Record is updated to agediscount table!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return "index";
        }

    }
}
