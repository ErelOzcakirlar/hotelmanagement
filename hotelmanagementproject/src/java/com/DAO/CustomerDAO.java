/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.DAO;

import com.entities.Customer;
import java.sql.Connection;
import java.sql.SQLException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author Erel
 */
@ManagedBean
@ApplicationScoped
public class CustomerDAO {

    /**
     * Creates a new instance of CustomerDAO
     */
    public CustomerDAO() {
    }

    public static Customer selectOne(String id) {
        Customer value;
        if (Customer.checkID(id)) {           
                try {
                    Connection conn = ConnectorManager.getConnection();
                    PreparedStatement stm = conn.prepareStatement("select * from customer where id = " + id);
                    ResultSet set = stm.executeQuery();
                    if (set.next()) {
                        value = new Customer(set.getString(1), set.getString(2));
                        value.Mail = set.getString(3);
                        value.Name = set.getString(4);
                        value.Surname = set.getString(5);
                        value.Phone = set.getString(6);
                        value.BirthDate = set.getDate(7);
                        
                    } else {
                        value = null;
                        
                    }
                    set.close();
                    stm.close();
                    conn.close();
                } catch (SQLException se) {
                    System.out.println("SQLException:" + se.getMessage());
                    value = null;
                }
        } else {
            value = new Customer();
            value.Name = "Geçersiz ID";
        }
        return value;
    }

    public static ArrayList<Customer> select() {
        ArrayList<Customer> value = new ArrayList<Customer>();       
            try {
               Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("select id from customer");
                ResultSet set = stm.executeQuery();
                ArrayList<String> IDs = new ArrayList<String>();
                while (set.next()) {
                    IDs.add(set.getString(1));
                }
                set.close();
                stm.close();
                conn.close();
                for(String s:IDs){
                    value.add(selectOne(s));
                }
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
            }
        return value;
    }
    
    public static ArrayList<Customer> search(String name){
        name = name.replace("'", "");
        name = name.replace("-", "");
        ArrayList<Customer> value = new ArrayList<Customer>();       
            try {
                Connection conn = ConnectorManager.getConnection();
                PreparedStatement stm = conn.prepareStatement("select id from customer where name like '%" + name + "%'");
                ResultSet set = stm.executeQuery();
                ArrayList<String> IDs = new ArrayList<String>();
                while (set.next()) {
                    IDs.add(set.getString(1));
                }
                set.close();
                stm.close();
                conn.close();
                for(String s:IDs){
                    value.add(selectOne(s));
                }
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
            }
        return value;
    }
    
    public static boolean insert(Customer c){
        boolean value = true;
            try {
                Connection conn = ConnectorManager.getConnection();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String birthDate = dateFormat.format(c.BirthDate);
                PreparedStatement stm = conn.prepareStatement("insert customer values('" + 
                        c.getID() + "','" +
                        c.getLeader().getID() + "','" +
                        c.Mail + "','" +
                        c.Name + "','" +
                        c.Surname + "','" +
                        c.Phone + "','" +
                        birthDate + "')");
                int rowCount = stm.executeUpdate();
                if( rowCount == 0){
                    value = false;
                }
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
                value = false;
            }
        return value;
    }
    
    public static boolean update(Customer c){
        boolean value = true;
       
            try {
                Connection conn = ConnectorManager.getConnection();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String birthDate = dateFormat.format(c.BirthDate);
                PreparedStatement stm = conn.prepareStatement("update customer set " +
                        "leaderId='"+ c.getLeader().getID() + "'," +
                        "name='" + c.Name + "'," +
                        "surname='" + c.Surname + "'," +
                        "phone='" + c.Phone + "'," +
                        "birthdate='" + birthDate + "'," + 
                        "email='" + c.Mail + "' where id='" + c.getID() + "'");
                int rowCount = stm.executeUpdate();
                if( rowCount == 0){
                    value = false;
                }
                stm.close();
                conn.close();
            } catch (SQLException se) {
                System.out.println("SQLException:" + se.getMessage());
                value = false;
            }      
        return value;
    }
}
