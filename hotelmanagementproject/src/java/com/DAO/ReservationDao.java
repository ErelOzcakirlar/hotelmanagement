package com.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.entities.Reservation;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

@ManagedBean(name = "reservationdao")
@SessionScoped
public class ReservationDao {

    private int id;
    private String customerId;
    private int billId;
    private int roomId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public ArrayList<Reservation> select() {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<Reservation> list = new ArrayList<Reservation>();

        String sql = "select * from reservation";

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                Reservation type = new Reservation();
                type.setId(result.getInt("id"));
                type.setRoomId(result.getInt("roomId"));
                type.setCustomerId(result.getString("customerId"));
                type.setBillId(result.getInt("billId"));
                list.add(type);
                /*
                 Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
                 List<Reservation> checkedItems = new ArrayList<Reservation>();
                 for (Reservation item : list) {
                 if (checked.get(item.getId())!= null) {
                 checkedItems.add(item);
                 type.delete(type.getId());
                 }	
                 }
                 */
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<Reservation> selectOne(int id) {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<Reservation> list = new ArrayList<Reservation>();

        String sql = "select * from reservation WHERE id=" + id;

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                Reservation type = new Reservation();
                type.setId(result.getInt("id"));
                type.setRoomId(result.getInt("roomId"));
                type.setCustomerId(result.getString("customerId"));
                type.setBillId(result.getInt("billId"));
                list.add(type);

                /*
                 Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
                 List<Reservation> checkedItems = new ArrayList<Reservation>();
                 for (Reservation item : list) {
                 if (checked.get(item.getId())!= null) {
                 checkedItems.add(item);
                 type.delete(type.getId());
                 }	
                 }
                 */
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<Reservation> selectBill(int billId) {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<Reservation> list = new ArrayList<Reservation>();

        String sql = "select * from reservation WHERE billId=" + billId;

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                Reservation type = new Reservation();
                type.setId(result.getInt("id"));
                type.setRoomId(result.getInt("roomId"));
                type.setCustomerId(result.getString("customerId"));
                type.setBillId(result.getInt("billId"));
                list.add(type);

                /*
                 Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
                 List<Reservation> checkedItems = new ArrayList<Reservation>();
                 for (Reservation item : list) {
                 if (checked.get(item.getId())!= null) {
                 checkedItems.add(item);
                 type.delete(type.getId());
                 }	
                 }
                 */
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public String insert() {
        int i = 0;

        if (!customerId.equals("")) {
            PreparedStatement ps = null;
            Connection con = null;
            try {
                con = ConnectorManager.getConnection();
                String sql = "INSERT INTO reservation(customerId, roomId, billId) VALUES('"+customerId+"',"+roomId+","+billId+")";
                ps = con.prepareStatement(sql); 
               

                i = ps.executeUpdate();
                System.out.println("Data Added Successfully");
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    con.close();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (i > 0) {
                return "index";
            } else {
                return "invalid";
            }
        } else {
            return "invalid";
        }
    }

    public String updateRoom(int roomId) {
        Connection dbConnection = null;
        Statement statement = null;

        String updateTableSQL = "UPDATE reservation"
                + " SET roomID =" + roomId
                + " WHERE id = 1";

        try {
            dbConnection = ConnectorManager.getConnection();
            statement = dbConnection.createStatement();

            System.out.println(updateTableSQL);

            // execute update SQL stetement
            statement.execute(updateTableSQL);

            System.out.println("Record is updated to reservation table!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return "index";
        }
    }

    public String updateBill(int billId) {
        Connection dbConnection = null;
        Statement statement = null;

        String updateTableSQL = "UPDATE reservation"
                + " SET billId =" + billId
                + " WHERE id = 1";

        try {
            dbConnection = ConnectorManager.getConnection();
            statement = dbConnection.createStatement();

            System.out.println(updateTableSQL);

            // execute update SQL stetement
            statement.execute(updateTableSQL);

            System.out.println("Record is updated to reservation table!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return "index";
        }
    }

    public String updateCustomer(int customerId) {
        Connection dbConnection = null;
        Statement statement = null;

        String updateTableSQL = "UPDATE reservation"
                + " SET customerId =" + customerId
                + " WHERE id = 1";

        try {
            dbConnection = ConnectorManager.getConnection();
            statement = dbConnection.createStatement();

            System.out.println(updateTableSQL);

            // execute update SQL stetement
            statement.execute(updateTableSQL);

            System.out.println("Record is updated to reservation table!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return "index";
        }
    }
    public Date formatMe(String target){      
         DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
         Date result = null;
        try {
            result = df.parse(target);
        } catch (ParseException ex) {
            Logger.getLogger(ReservationDao.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         return result;
    }
}
