package com.DAO;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.DAO.ConnectorManager;
import com.entities.RoomType;

@ManagedBean(name = "roomtypedao")
@SessionScoped
public class RoomTypeDao {

    private int id;
    private int evenBed;
    private int oddBed;
    private int bunkBed;
    private int bedCount;
    private BigDecimal prizePerDay;
    private String description;
    private String photo1url;
    private String photo2url;
    private String photo3url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEvenBed() {
        return evenBed;
    }

    public void setEvenBed(int evenBed) {
        this.evenBed = evenBed;
    }

    public int getOddBed() {
        return oddBed;
    }

    public void setOddBed(int oddBed) {
        this.oddBed = oddBed;
    }

    public int getBunkBed() {
        return bunkBed;
    }

    public void setBunkBed(int bunkBed) {
        this.bunkBed = bunkBed;
    }

    public int getBedCount() {
        return bedCount;
    }

    public void setBedCount(int bedCount) {
        this.bedCount = bedCount;
    }

    public BigDecimal getPrizePerDay() {
        return prizePerDay;
    }

    public void setPrizePerDay(BigDecimal prizePerDay) {
        this.prizePerDay = prizePerDay;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto1url() {
        return photo1url;
    }

    public void setPhoto1url(String photo1url) {
        this.photo1url = photo1url;
    }

    public String getPhoto2url() {
        return photo2url;
    }

    public void setPhoto2url(String photo2url) {
        this.photo2url = photo2url;
    }

    public String getPhoto3url() {
        return photo3url;
    }

    public void setPhoto3url(String photo3url) {
        this.photo3url = photo3url;
    }

    
    public ArrayList<RoomType> select() {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<RoomType> list = new ArrayList<RoomType>();

        String sql = "select * from roomtype";

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                RoomType type = new RoomType();
                type.setId(result.getInt("id"));
                type.setEvenBed(result.getInt("evenBed"));
                type.setOddBed(result.getInt("oddBed"));
                type.setBunkBed(result.getInt("bunkBed"));
                type.setBedCount(result.getInt("bedCount"));
                type.setPrizePerDay(result.getBigDecimal("prizePerDay"));
                type.setDescription(result.getString("description"));
                type.setPhoto1url(result.getString("photo1url"));
                type.setPhoto2url(result.getString("photo2url"));
                type.setPhoto3url(result.getString("photo3url"));
                list.add(type);

                /*Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
                List<RoomType> checkedItems = new ArrayList<RoomType>();
                for (RoomType item : list) {
                    if (checked.get(item.getId()) != null) {
                        checkedItems.add(item);
                        type.delete(type.getId());
                    }
                }*/
            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<RoomType> selectOne(int id) {
        PreparedStatement state = null;
        ResultSet result = null;
        ArrayList<RoomType> list = new ArrayList<RoomType>();

        String sql = "select * from roomtype WHERE id=" + id;

        try {
            Connection conn = ConnectorManager.getConnection();
            state = conn.prepareStatement(sql);
            result = state.executeQuery();
            while (result.next()) {
                RoomType type = new RoomType();
                type.setId(result.getInt("id"));
                type.setEvenBed(result.getInt("evenBed"));
                type.setOddBed(result.getInt("oddBed"));
                type.setBunkBed(result.getInt("bunkBed"));
                type.setBedCount(result.getInt("bedCount"));
                type.setPrizePerDay(result.getBigDecimal("prizePerDay"));
                type.setDescription(result.getString("description"));
                type.setPhoto1url(result.getString("photo1url"));
                type.setPhoto2url(result.getString("photo2url"));
                type.setPhoto3url(result.getString("photo3url"));
                list.add(type);

                /*Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
                List<RoomType> checkedItems = new ArrayList<RoomType>();
                for (RoomType item : list) {
                    if (checked.get(item.getId()) != null) {
                        checkedItems.add(item);
                        type.delete(type.getId());
                    }
                }*/

            }
            result.close();
            state.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public String insert() {int i = 0;

        if (true) {
            PreparedStatement ps = null;
            Connection con = null;
            try {
                con = ConnectorManager.getConnection();
                String sql = "INSERT INTO roomtype(id, evenBed, oddBed, bunkBed, bedCount, prizePerDay, description, photo1url, photo2url, photo3url) VALUES(0,?,?,?,?,?,?,?,?,?)";
                ps = con.prepareStatement(sql);
           
                ps.setInt(1, evenBed);
                ps.setInt(2, oddBed);
                ps.setInt(3, bunkBed);
                ps.setInt(4, bedCount);
                ps.setBigDecimal(5, prizePerDay);
                ps.setString(6, description);
                ps.setString(7, photo1url);
                ps.setString(8, photo2url);
                ps.setString(9, photo3url);
                
                i = ps.executeUpdate();
                System.out.println("Data Added Successfully");
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    con.close();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (i > 0) {
                return "index";
            } else {
                return "invalid";
            }
        } else {
            return "invalid";
        }
    }

    public String update() {
        Connection dbConnection = null;
        PreparedStatement ps = null;

        

        try {
            dbConnection = ConnectorManager.getConnection();

            String sql = "UPDATE roomtype SET evenBed='"+evenBed+"', oddBed='"+oddBed+"', bunkBed='"+bunkBed+"',bedCount='"+bedCount+"', prizePerDay='"+prizePerDay+"', description='"+description+"', photo1url='"+photo1url+"', photo2url='"+photo2url+"', photo3url='"+photo3url+"' WHERE id='"+id+"'";
                 ps = dbConnection.prepareStatement(sql);
         
            System.out.println(sql);

            // execute update SQL stetement
            ps.execute(sql);

            System.out.println("Record is updated to roomtype table!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return "index";
        }
        

    }
    
    public static String getName(int typeId){
        String value;
        try {
            Connection conn = ConnectorManager.getConnection();
            PreparedStatement stm = conn.prepareStatement("select description from roomtype where id = " + typeId);
            ResultSet set = stm.executeQuery();
            if (set.next()) {
                value = set.getString(1);
            } else {
                set.close();
                stm.close();
                conn.close();
                value = null;
            }
        } catch (SQLException se) {
            System.out.println("SQLException:" + se.getMessage());
            value = null;
        }
        return value;
    }
}
